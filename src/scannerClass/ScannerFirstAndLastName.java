package scannerClass;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your first name?");

        String first = input.next();

        System.out.println("Please enter your last name?");

        String last = input.nextLine();

        System.out.println("Your full name is " + first + " " + last);


        int num1, num2, num3;

        System.out.println("Please enter number 1?");
        num1 = input.nextInt();
        System.out.println("Please enter number 2?");
        num2 = input.nextInt();
        System.out.println("Please enter number 3?");
        num3 = input.nextInt();
        System.out.println("The sum of the numbers you entered " + num1 + num2 + num3);



    }
}
