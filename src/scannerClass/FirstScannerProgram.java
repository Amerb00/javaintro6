package scannerClass;

import java.util.Scanner;

public class FirstScannerProgram {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //.net method
        System.out.println("Please enter your name.");
        String name = input.next(); // Lets user type theyre answer and it will be stored in "name"
        input.nextLine();
        System.out.println("The user's name is: " + name);

        //.nextLine method
        System.out.println("Please enter your first and last name");
        String fullName = input.nextLine();

        System.out.println("The users full name is: " + fullName);

        //nextInt method
        System.out.println("Please enter a number.");
        int number = input.nextInt();
        System.out.println("The number you chose is : " + number);


    }
}
