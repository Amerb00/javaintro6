package scannerClass;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter your first name.");
        String name = input.nextLine();

        System.out.println("Enter your address.");
        String address = input.nextLine();

        System.out.println("Enter your favorite number.");
        int favNum = input.nextInt();

        System.out.println(name + "'s" + " address is " + address + " and their fav number is " + favNum);









    }
}
