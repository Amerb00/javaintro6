package concatenation;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println("\n-------------TASK-1-------------\n");
        System.out.println("Hello world");
        System.out.println("Hello" + "world");
        System.out.println("Hello " + "world");
        System.out.println("Hello" + " world");
        System.out.println("Hello" + " " + "world");
        System.out.println("Today" + " " + "is" + " " + "Sunday");

        System.out.println("\t" + "12" + "\t\nab");


    }
}
