package arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {

        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam", "Louie"},
                {"Dima", "Lesia", "Pinar"}
        };

        System.out.println(students[0].length);

        for (int i = 0; i < students.length; i++) {

            for (int j = 0; j < students[i].length; j++) {
                System.out.println(students[i][j]);
            }

        }
    }


}
