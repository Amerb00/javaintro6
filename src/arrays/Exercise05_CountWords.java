package arrays;

import utilities.ScannerHelper;

public class Exercise05_CountWords {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();
        int count = 0;

        for (String word: str.split(" ")) {
            if (!word.isEmpty()) count++;
        }
        System.out.println(count);

        System.out.println(str.split(" ").length);


    }
}
