package arrays;

import java.util.Arrays;

public class UnderstandingArray {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};

        //The number elements in the array
        int sizeOfArray = cities.length;
        System.out.println(sizeOfArray); // 3

        // Get particular element for the array
        System.out.println(cities[1]); // Miami
        System.out.println(cities[0]); // Chicago
        System.out.println(cities[2]); // Toronto

        // HOW TO PRINT THE ARRAY -> [Chicago, Miami, Toronto]
        System.out.println(Arrays.toString(cities));

        //HOW TO LOOP AN ARRAY

        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        for (String element :cities) {
            System.out.println(element);
        }
        String[] words = new String[6];
        System.out.println(Arrays.toString(words));
    }
}
