package arrays;

public class Exercise01_CountNumber {
    public static void main(String[] args) {
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        int sum = 0;
        for (int num:numbers) {
            if(Math.abs(num - 9) <= 2 && num < 9) System.out.println(num);
            else if (Math.abs(num - 9) <= 2) System.out.println(num);
        }
//        System.out.println(sum);
    }
}
