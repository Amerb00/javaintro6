package arrays;

import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {

        // Declare a String array called as countries and assign size of 3
        String[] countries = new String[3];

        // assign "Spain" to index of 1
        countries[1] = "Spain";


        // Print the value at index of 1 and 2
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        // Assign "Belgium" at index of 0 and "Italy" at index of 2
        countries[0] = "Belgium";
        countries[2] =  "Italy";

        // print the array
        System.out.println(Arrays.toString(countries));

        // sort this array and print it
        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }

        for (String country:countries) {
            System.out.println(country);
        }

        int count = 0;
        for (String country : countries) {
            if (country.length() == 5) count++;
        }
        System.out.println(count);

        int counter = 0;
        for (String country : countries) {
            if (country.contains("i") || country.contains("I")) counter++;
        }
        System.out.println(counter);






    }
}
