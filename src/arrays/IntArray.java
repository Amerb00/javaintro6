package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {

        int[] num = new int[6];
        num[0] = 5;
        num[2] = 15;
        num[4] = 25;
        System.out.println(Arrays.toString(num));


        for (int i = 0; i < num.length; i++) {
            System.out.println(num[i]);
        }


        for (int numbers:num) {
            System.out.println(numbers);
        }



    }
}
