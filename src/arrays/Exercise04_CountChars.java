package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();

        char[] chars = str.toCharArray();

        int count = 0;
        for (char character :  str.toCharArray()) {
            if (Character.isLetter(character)) count++;
        }
        System.out.println(count);
    }
}
