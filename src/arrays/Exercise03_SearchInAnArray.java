package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Mouse", "Keyboard", "ipad"};

        boolean hasM = false;

        for (String object : objects) {
            if (object.contains("Mouse")) {
                hasM= true;
                break;
            }
        }
        System.out.println(hasM);

        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);




    }
}
