package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {


        double[] number = {5.5, 6,19.3,25};

        System.out.println(Arrays.toString(number));


        System.out.println("The length is " + number.length);


        for (double num : number) {

            System.out.println(num);

        }
    }
}
