package date_time;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class _01_LocalDateTime {
    public static void main(String[] args) {

        System.out.println("-----------LocalDate----------");
        LocalDate localeDate = LocalDate.now();

        System.out.println(localeDate);

        System.out.println(localeDate.getYear());

        System.out.println(localeDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));


        System.out.println("-----------LocalTime----------");

        LocalTime currentTime = LocalTime.now();

        System.out.println(currentTime);
        System.out.println("-----------LocalDateTime----------");

        LocalDateTime currentDateTime = LocalDateTime.now();

        System.out.println(currentDateTime);



    }
}
