package mathClass;

import java.util.Scanner;

public class FindMin {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);


        System.out.println("Please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        int min = Math.min(num1,num2);
        int min2 = Math.min(min,num3);


        System.out.println("The min of " + num1 + ", " + num2 + ", " + num3 + " is: " + min2);


    }
}
