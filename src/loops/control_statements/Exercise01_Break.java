package loops.control_statements;

import utilities.ScannerHelper;

public class Exercise01_Break {

    public static void main(String[] args) {
        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();


        int max = Math.max(num1,num2);
        int min = Math.min(num1,num2);

        for (int i = max; i >= min ; i--) {

            if(i < 10)
                break;
            System.out.println(i);
        }
    }
}
