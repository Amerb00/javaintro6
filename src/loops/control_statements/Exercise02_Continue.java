package loops.control_statements;

public class Exercise02_Continue {
    public static void main(String[] args) {


        for (int i = 1; i <= 100 ; i++) {
            if (i % 13 == 0)
                continue;
            System.out.println(i);

        }
    }
}
