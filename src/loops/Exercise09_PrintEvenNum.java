package loops;

import utilities.ScannerHelper;

public class Exercise09_PrintEvenNum {
    public static void main(String[] args) {



        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        int end = Math.max(num1,num2);
        int start = Math.min(num1,num2);


        for (int i = start; i < end ; i++) {
            if(i % 2 == 0) System.out.println(i);

        }
    }
}
