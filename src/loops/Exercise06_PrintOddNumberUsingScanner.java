package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise06_PrintOddNumberUsingScanner {
    public static void main(String[] args) {


        int num = ScannerHelper.getNumber();

        for (int i = 0; i <= num; i++) {
           if (i % 2 != 0)
            System.out.println(i);

        }

    }
}
