package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintEachChar {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        int count = 0;
        for (int i = 0; i < str.length() ; i++) {
            if(str.charAt(i) == 'a' ||str.charAt(i) == 'A' ) count++;
        }
        System.out.println(count);
    }
}
