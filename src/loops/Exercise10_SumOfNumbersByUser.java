package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {


        int sum = 0;

        for (int i = 1; i <= 5 ; i++) {
            sum += ScannerHelper.getNumber();
        }
        System.out.println(sum);


    }
}
