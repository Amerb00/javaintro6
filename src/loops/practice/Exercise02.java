package loops.practice;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        int num = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        for (int i = Math.min(num,num2); i <= Math.max(num,num2) ; i++) {
            if (i != 5)
                System.out.println(i);

        }
    }
}
