package loops.practice;

import utilities.ScannerHelper;

public class Exercise03 {
    public static void main(String[] args) {

        String name = ScannerHelper.getString().toLowerCase();
        int count = 0;

        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == 'a'||name.charAt(i) == 'e'||name.charAt(i) == 'i'||name.charAt(i) == 'o'||name.charAt(i) == 'u')
                count++;
        }
        System.out.println(count);






    }
}
