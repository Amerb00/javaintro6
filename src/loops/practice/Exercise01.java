package loops.practice;

import utilities.ScannerHelper;

public class Exercise01 {

    public static void main(String[] args) {
        int num;
        int counter = 0;
        do {
            if (counter < 1)  System.out.println("This number is not more than or equals 10");
            num = ScannerHelper.getNumber();
            counter++;

        }
        while (num < 10);

        System.out.println("This number is more than or equals 10");

    }





}
