package loops.practice;

import utilities.ScannerHelper;

public class Exercise05 {
    public static void main(String[] args) {

        int num;
        int num2 =0;
        int sum=0;

        do {
            num = ScannerHelper.getNumber();
            if (num > 100) System.out.println("This number is already more than 100");
            if (num < 100)
                num2 = ScannerHelper.getNumber();
            sum = num + num2;
        }
        while (sum < 100);
        System.out.println("Sum of the entered number is at least 100");


    }
}
