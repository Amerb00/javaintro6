package string_Methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("What is your favorite book?");
        String str1 = input.nextLine();

        System.out.println("What is your favorite quote from book?");
        String str2 = input.nextLine();

        int bookL = str1.length();
        int quoteL = str2.length();

        System.out.println(bookL);
        System.out.println(quoteL);

    }
}
