package string_Methods;

import utilities.ScannerHelper;

public class _12_startsAndEndsWith_Method {
    public static void main(String[] args) {


        String word = ScannerHelper.getString();

        System.out.println(word.startsWith("a") && word.endsWith("e"));

    }
}
