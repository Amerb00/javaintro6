package string_Methods;

import java.util.Arrays;

public class _17_split_Method {
    public static void main(String[] args) {
        String str = "Hello World";
        String str2 = "John - Doe - 11/11/1999 - Johndoe@gmail.com - Chicago";

        System.out.println(Arrays.toString(str.split(" ")));
        System.out.println(Arrays.toString(str2.split(" - ")));

    }
}
