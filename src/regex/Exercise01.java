package regex;

import utilities.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise01 {
    public static void main(String[] args) {


        String input = ScannerHelper.getString();

        if (Pattern.matches("[A-Za-z] +",input)) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 Characters long and can only contains letters and numbers");


    }
}
