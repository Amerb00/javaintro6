package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise02 {
    public static void main(String[] args) {

        int count = 0;

        String word = ScannerHelper.getString();
        Pattern pattern = Pattern.compile("[A-Za-z]{2}");
        Matcher matcher = pattern.matcher(word);


        while (matcher.find()){
            System.out.println(matcher.group());
            count++;
        }
        System.out.println("This sentence contains " + count +" words.");


    }
}
