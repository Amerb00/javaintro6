package regex;

import utilities.ScannerHelper;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise03 {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();


        System.out.println(str.replaceAll("[^aeiouAEIOU]", ""));
        System.out.println(str.replaceAll("[^aeiouAEIOU]", "").length());


    }
}
