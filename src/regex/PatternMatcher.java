package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {

        String input = ScannerHelper.getString();
//
//        System.out.println(Pattern.matches("[a-z0-9_-]{3,10}", input));




        Pattern pattern = Pattern.compile("[^aeiouAEIOU]+");
        Matcher matcher = pattern.matcher(input);


        System.out.println(matcher.matches());


    }
}
