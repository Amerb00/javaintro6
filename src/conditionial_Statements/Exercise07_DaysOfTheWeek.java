package conditionial_Statements;

import java.util.Random;

public class Exercise07_DaysOfTheWeek {
    public static void main(String[] args) {
        Random random = new Random();

        int num1 = random.nextInt(7);

        if (num1 == 0){
            System.out.println("Sunday");
        } else if (num1 == 1) {
            System.out.println("Monday");

        }else if (num1 == 2) {
            System.out.println("Tuesday");

        }else if (num1 == 3) {
            System.out.println("Wednesday");

        }else if (num1 == 4) {
            System.out.println("Thursday");

        }else if (num1 == 5) {
            System.out.println("Friday");

        }else if (num1 == 6) {
            System.out.println("Saturday");

        }

        switch (num1){
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
        }



    }








}
