package conditionial_Statements;

import java.util.Scanner;

public class Exercise04_RetirmentAge {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your age?");

        int num1 = input.nextInt();

        if (num1 >= 55){
            System.out.println("It is you time to get retired!");
        }else if (num1 == 54) {
            System.out.println("You have " + (55 - num1) + " year to be retired");
        }else {
            System.out.println("You have " + (55 - num1) + " years to be retired");
        }


    }
}
