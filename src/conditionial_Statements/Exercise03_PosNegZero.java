package conditionial_Statements;

import java.util.Scanner;

public class Exercise03_PosNegZero {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        System.out.println("Enter a number");
        int num1 = input.nextInt();


        if (num1 > 0){
            System.out.println("POSITIVE");
        }else if (num1 < 0){
            System.out.println("NEGATIVE");
        }else
        System.out.println("ZERO");

    }
}
