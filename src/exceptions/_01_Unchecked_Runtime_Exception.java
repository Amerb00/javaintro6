package exceptions;

public class _01_Unchecked_Runtime_Exception {
    public static void main(String[] args) {

        String name = "john";

        int[] numbers = {10,15,20};

        System.out.println(numbers[5]);//ArrayIndexOutOfBoundsException
        System.out.println(name.charAt(-7)); // StringIndexOutOfBoundsException
        String address = null;

        System.out.println(address.toLowerCase());

        System.out.println("End of the program");


    }
}
