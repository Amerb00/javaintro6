package exceptions;

public class Exercise {
    public static void main(String[] args) {

        System.out.println(checkAge(-2));

        System.out.println(isCheckInHours(1));

    }

    public static boolean checkAge(int age){

        if (age< 1 || age >=120) throw new RuntimeException();

        return age > 16;

    }
    public static boolean isCheckInHours(int day){


        if (day >= 1 && day <= 7) return true;
        else throw new RuntimeException("The input does not represent any day!!");

        }


    }

