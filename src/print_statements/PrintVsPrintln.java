package print_statements;

public class PrintVsPrintln {
    public static void main(String[] args){
        //println
        System.out.println("Hello World");
        System.out.println("Today is Sunday");
        System.out.println(" John Doe");

        //print
        System.out.println("Hello World");
        System.out.println("Today is Sunday");
        System.out.println(" John Doe");
    }
}
