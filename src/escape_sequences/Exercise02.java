package escape_sequences;

public class Exercise02 {
    public static void main(String[] args) {
        System.out.println("\n-------------TASK-2-------------");
        System.out.println("I like \"Sunday\" and apple.");

        System.out.println("\n-------------TASK-3-------------");
        System.out.println("My fav fruits are \"Kiwi\" and \"Orange\"");

    }
}
