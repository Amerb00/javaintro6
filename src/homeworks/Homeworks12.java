package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;

public class Homeworks12 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(noDigit("java"));
        System.out.println(noDigit("123java"));
        System.out.println(noDigit("123hello world123"));
        System.out.println(noDigit("123Tech234Global123"));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(noVowel(""));
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("JAVA"));
        System.out.println(noVowel("123$"));
        System.out.println(noVowel("TechGlobal"));
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));
        System.out.println(hasUpperCase("$125.0"));
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(middleInt(1,1,1));
        System.out.println(middleInt(1,2,2));
        System.out.println(middleInt(5,5,8));
        System.out.println(middleInt(5,3,5));
        System.out.println(middleInt(-1,25,10));
        //________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13, 13, 13})));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{0,5})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5,0,6})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{})));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        System.out.println(Arrays.toString(categorizeCharacters("   ")));
        System.out.println(Arrays.toString(categorizeCharacters("abc123$*%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));


    }

    public static String noDigit(String str){
        return str.replaceAll("[0-9]", "");
    }

    public static String noVowel(String str){
        return str.replaceAll("[AEIOUaeiou]", "");
    }

    public static int sumOfDigits(String str){
        String num = str.replaceAll("[^1-9]","");

        int sum = 0;
        for (int i = 0; i < num.length(); i++) {
            sum += Character.getNumericValue(num.charAt(i));
        }
        return sum;


    }

    public static boolean hasUpperCase(String str){
        for (int i = 0; i <= str.length()-1; i++) {
            if (Character.isUpperCase(str.charAt(i))) return true;
        }
        return false;
    }

    public static int middleInt(int num1,int num2,int num3){

        int[] nums = new int[] {num1,num2,num3};
        Arrays.sort(nums);
        return nums[1];

    }

    public static int[] no13(int[] arr){

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 13) arr[i] = 0;
        }
        return arr;
    }

    public static int[] arrFactorial(int[] arr){

        for (int i = 0; i < arr.length ; i++) {
            int fac = arr[i];
            if (arr[i] == 0){
                arr[i] = 1;
                continue;
            }
                for (int j = 2; j < arr[i]; j++) {
                    fac *= j;
                }
                arr[i] = fac;
            }
        return arr;
        }

    public static String[] categorizeCharacters(String str){

        String[] arr = new String[]{"","",""};

        for (int i = 0; i <= str.length()-1 ; i++) {

            if (Character.isLetter(str.charAt(i))) arr[0] += str.charAt(i);
            else if (Character.isDigit(str.charAt(i))) arr[1] += str.charAt(i);
            else if (!Character.isLetterOrDigit(str.charAt(i)) && !Character.isWhitespace(str.charAt(i))) arr[2] += str.charAt(i);


        }
        return arr;



    }

}

