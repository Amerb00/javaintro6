package homeworks;

import utilities.ScannerHelper;

public class Homeworks04 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        String name = ScannerHelper.getFullName();

        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.charAt(name.length() - 1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0, 3));
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length() - 3));

        if (name.charAt(0) == 'a' || name.charAt(0) == 'A') System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");


        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        String address = ScannerHelper.getAddress().toLowerCase();


        if (address.contains("chicago")) System.out.println("You are in the club");
        else if (address.contains("des plains")) System.out.println("You are welcome to join the club");
        else System.out.println("Sorry, you will never be in the club");


        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        String country = ScannerHelper.getFavCountry().toUpperCase();


        if (country.contains("A") && country.contains("I")) System.out.println("A and I are there");
        else if (country.contains("A")) System.out.println("A is there");
        else if (country.contains("I")) System.out.println("I is there");
        else System.out.println("A and i are not there");

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        String str = "   Java is FUN   ";

        String newStr = str.trim().toLowerCase();

        String firstWord = newStr.substring(0, 4);
        String secondWord = newStr.substring(5, 7);
        String thirdWord = newStr.substring(8);

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);


    }
}
