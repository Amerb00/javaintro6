package homeworks;

import java.sql.SQLOutput;

public class Homeworks01 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK1----------");
        /*FIND THE BINARY REPRESENTATION OF BELOW WORDS
        SELENIUM = 01010011 01000101 01001100 01000101 01001010 01001001 01010101 01001101
        S = 83 = 01010011
        E = 69 = 01000101
        L = 76 = 01001100
        E = 69 = 01000101
        N = 78 = 01001010
        I = 73 = 01001001
        U = 85 = 01010101
        M = 77 = 01001101
        */

        System.out.println("\n----------TASK2----------");
        /*
        01001101    = 77  = M
        01101000    = 104 = h
        01111001    = 121 = y
        01010011    = 83  = S
        01101100    = 108 = l
        */

        System.out.println("\n----------TASK3----------");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It's not the load that breaks you down, it's the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");

        System.out.println("\n----------TASK4----------");
        System.out.println("\tJava is easy to write and easy to run—this is the foundational\n " +
                "strength of Java and why many developers program in it. When you\n" +
                "write Java once, you can run it almost anywhere at any time.\n\n" +
                "\tJava can be used to create complete applications that can run on\n" +
                "a single computer or be distributed across servers and clients in a\nnetwork.\n\n " +
                "\tAs a result, you can use it to easily build mobile applications or\n " +
                "run-on desktop applications that use different operating systems and\n " +
                "servers, such as Linux or Windows.");

        System.out.println("\n----------TASK5----------");

        byte myAge = 19;
        System.out.println("My age is " + myAge + ".");

        byte myFavoriteNumber = 2;
        System.out.println("My favorite number is " + myFavoriteNumber + ".");

        double myHeight = 5.11;
        System.out.println("My height is " + myHeight + ".");

        short myWeight = 140;
        System.out.println("My weight is " + myWeight + ".");

        char myFavoriteLetter = 'B';
        System.out.println("My favorite letter is " + myFavoriteLetter + ".");


    }
}
