package homeworks;

import java.util.Scanner;

public class Homeworks03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        int num1, num2, num3, num4, num5;

        System.out.println("Enter 2 numbers.");
        num1 = input.nextInt();
        num2 = input.nextInt();

        System.out.println("the difference between numbers is = " + (Math.abs(num1 - num2)));

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        System.out.println("Enter 5 numbers.");
        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();
        num4 = input.nextInt();
        num5 = input.nextInt();

        int max = Math.max(Math.max(Math.max(num1, num2), Math.max(num3, num4)), num5);
        int min = Math.min(Math.min(Math.min(num1, num2), Math.min(num3, num4)), num5);

        System.out.println("Max value = " + max);
        System.out.println("Min value = " + min);

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        int random1 = (int) (Math.random() * 51 + 50);
        int random2 = (int) (Math.random() * 51 + 50);
        int random3 = (int) (Math.random() * 51 + 50);


        System.out.println("Number 1 = " + random1);
        System.out.println("Number 2 = " + random2);
        System.out.println("Number 3 = " + random3);
        System.out.println("The sum of numbers is = " + (random1 + random2 + random3));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        double alex = 125;
        double mike = 220;
        double give = 25.5;


        System.out.println("Alex's money: $" + (alex - give));
        System.out.println("Mike's money: $" + (mike + give));

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        double price = 390;
        double save = 15.60;

        System.out.println((int) (price / save));

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        String s1 = "5", s2 = "10";

        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);

        System.out.println("Sum of 5 and 10 is = " + (n1 + n2));
        System.out.println("Product of 5 and 10 is = " + (n1 * n2));
        System.out.println("Division of 5 and 10 is = " + (n1 / n2));
        System.out.println("Subtraction of 5 and 10 is = " + (n1 - n2));
        System.out.println("Remainder of 5 and 10 is = " + (n1 % n2));

        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");

        String s3 = "200", s4 = "-50";

        int n3 = Integer.parseInt(s3);
        int n4 = Integer.parseInt(s4);

        System.out.println("The greatest value is = " + (Math.max(n3, n4)));
        System.out.println("The smallest value is = " + (Math.min(n3, n4)));
        System.out.println("The average is = " + ((n3 + n4) / 2));
        System.out.println("The absolute difference is = " + (Math.abs(n3 - n4)));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        double days = .96;

        System.out.println((int) (24 / days) + " days");
        System.out.println((int) (168 / days) + " days");
        System.out.println("$" + (150 * days));

        //____________________________________________________________TASK9_____________________________________________________________________________
        System.out.println("----------TASK9----------");


        double jess = 1250 / 62.5;

        System.out.println((int) jess);

        //____________________________________________________________TASK10_____________________________________________________________________________
        System.out.println("----------TASK10----------");

        int option1 = (int) (14_265 / 475.5);
        int option2 = 14_265 / 951;

        System.out.println("Option 1 will take " + option1 + " months");
        System.out.println("Option 2 will take " + option2 + " months");

        //____________________________________________________________TASK11_____________________________________________________________________________
        System.out.println("----------TASK11----------");

        System.out.println("Enter 2 numbers.");
        num1 = input.nextInt();
        num2 = input.nextInt();

        System.out.println((double) num1 / num2);

        //____________________________________________________________TASK12_____________________________________________________________________________
        System.out.println("----------TASK12----------");

        int rando1 = (int) (Math.random() * 101);
        int rando2 = (int) (Math.random() * 101);
        int rando3 = (int) (Math.random() * 101);


        /*
        If else statements check if any of the three numbers
        are less than or equal to 25 if so false is printed
        if not true is printed
        */
        if (rando1 <= 25) {
            System.out.println("false");
        } else if (rando2 <= 25) {
            System.out.println("false");
        } else if (rando3 <= 25) {
            System.out.println("false");
        } else {
            System.out.println("true");
        }


        //____________________________________________________________TASK13_____________________________________________________________________________
        System.out.println("----------TASK13----------");

        System.out.println("Enter a number between 1-7");

        num1 = input.nextInt();


        /*
         The switch statements reads the users input and returns a print statement for
         only numbers between 1-7, if anything else it will return "Error"
        */

        switch (num1) {
            case 1:
                System.out.println("The number entered returns MONDAY");
                break;
            case 2:
                System.out.println("The number entered returns TUESDAY");
                break;
            case 3:
                System.out.println("The number entered returns WEDNESDAY");
                break;
            case 4:
                System.out.println("The number entered returns THURSDAY");
                break;
            case 5:
                System.out.println("The number entered returns FRIDAY");
                break;
            case 6:
                System.out.println("The number entered returns SATURDAY");
                break;
            case 7:
                System.out.println("The number entered returns SUNDAY");
                break;
            default:
                System.out.println("Error");
        }

        //____________________________________________________________TASK14_____________________________________________________________________________
        System.out.println("----------TASK14----------");

        System.out.println("Tell me your exam results?");

        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();

        int average = (num1 + num2 + num3) / 3;

        if (average >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }

        //____________________________________________________________TASK15_____________________________________________________________________________
        System.out.println("----------TASK15----------");


        System.out.println("Enter 3 numbers");
        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();


        if (num2 == num3 && num1 == num3) {      //Checks if all three are the same
            System.out.println("TRIPLE MATCH");
        } else if (num1 == num2) {              //Checks if num1 and num2 are the same
            System.out.println("DOUBLE MATCH");
        } else if (num2 == num3) {                //Checks if num2 and num3 are the same
            System.out.println("DOUBLE MATCH");
        } else if (num1 == num3) {                //Checks if num1 and num3 are the same
            System.out.println("DOUBLE MATCH");
        } else {                                 //If all the numbers are different prints "NO MATCH"
            System.out.println("NO MATCH");
        }


    }
}
