package homeworks;

import java.util.Arrays;

public class Homeworks18 {
    public static void main(String[] args) {


        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        int[] arr = {1,5,10};
        System.out.println(Arrays.toString(doubleOrTriple(arr,true)));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(splitString("Java",2));
        System.out.println(splitString("JavaScript",5));
        System.out.println(splitString("Hello",3));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(countPalindrome("Mom and Dad"));
        System.out.println(countPalindrome("Kayak races attracts racecar drivers"));

    }


    public static int[] doubleOrTriple(int[] arr, boolean val) {
        int multiplier = val ? 2 : 3;
        int[] modifiedArray = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            modifiedArray[i] = arr[i] * multiplier;
        }

        return modifiedArray;
    }
    public static String splitString(String str, int num) {
        if (str.length() % num != 0) {
            return "";
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < str.length(); i += num) {
            String substring = str.substring(i, i + num);
            result.append(substring).append(" ");
        }

        return result.toString().trim();
    }
    public static int countPalindrome(String str) {
        String[] words = str.split("\\s+");
        int count = 0;

        for (String word : words) {
            String reversed = new StringBuilder(word.toLowerCase()).reverse().toString();
            if (word.equalsIgnoreCase(reversed)) {
                count++;
            }
        }

        return count;
    }


}
