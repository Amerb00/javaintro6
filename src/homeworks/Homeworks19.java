package homeworks;

import java.util.Arrays;

public class Homeworks19 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(sum(new int[]{1,5,10},true));
        System.out.println(sum(new int[]{3,7,2,5,10},false));

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(nthChars("Java",2));
        System.out.println(nthChars("Java",3));
        System.out.println(nthChars("JavaScript",5));
        System.out.println(nthChars("Hi",4));


        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(canFormString("Hello","Hi"));
        System.out.println(canFormString("programming","gaming"));
        System.out.println(canFormString("halogen","hello"));
        System.out.println(canFormString("CONVERSATION","voices rant on"));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        System.out.println(isAnagram("Apple","Peach"));
        System.out.println(isAnagram("astronomer","moon starer"));
        System.out.println(isAnagram("listen","silent"));
        System.out.println(isAnagram("CINEMA","iceman"));

    }

    public static int sum(int[] arr, boolean isEven) {
        int sum = 0;

        if (isEven) {
            for (int i = 0; i < arr.length; i += 2) {
                sum += arr[i];
            }
        } else {
            for (int i = 1; i < arr.length; i += 2) {
                sum += arr[i];
            }
        }

        return sum;
    }

    public static String nthChars(String input, int n) {
        if (input == null || n <= 0 || input.length() < n) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i += n) {
            result.append(input.charAt(i));
        }

        return result.toString();
    }

    public static boolean canFormString(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }

        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        return Arrays.equals(arr1, arr2);
    }

    public static boolean isAnagram(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }

        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        if (str1.length() != str2.length()) {
            return false;
        }

        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        return Arrays.equals(arr1, arr2);
    }


}
