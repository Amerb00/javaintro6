package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homeworks07 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10,23,67,23,78));

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue","Brown","Red","White","Black","Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        numbers = new ArrayList<>(Arrays.asList(23,-34,-56,0,89,100));

        System.out.println(numbers);
        Collections.sort(numbers);
        System.out.println(numbers);

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");


        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul","Berlin","Madrid","Paris"));

        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man","Iron Man","Black Panther","Deadpool","Captain America"));

        System.out.println(marvel);

        boolean wolv = false;

        for (String s : marvel) {
            if (s.contains("Wolverine")) wolv = true;
        }
        System.out.println(wolv);

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        marvel = new ArrayList<>(Arrays.asList("Hulk","Black Widow","Captain America","Iron Man"));
        Collections.sort(marvel);
        System.out.println(marvel);

        int count = 0;

        for (String s : marvel) {
            if (s.contains("Hulk")) count++;
            if (s.contains("Iron Man")) count++;
        }
        System.out.println(count == 2);

        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");

        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A','x','$','%','9','*','+','F','G'));

        System.out.println(characters);

        for (Character character : characters) {
            System.out.println(character);
        }
        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        ArrayList<String> tech = new ArrayList<>(Arrays.asList("Desk","Laptop","Mouse","Monitor","Mouse-Pad","Adapter"));

        System.out.println(tech);
        Collections.sort(tech);
        System.out.println(tech);
        int countM = 0;
        int countAe = 0;

        for (String s : tech) {
            if (s.trim().toLowerCase().startsWith("m")) countM++;
            if (!s.toLowerCase().contains("a") && !s.toLowerCase().contains("e")) countAe++;
        }

        System.out.println(countM);
        System.out.println(countAe);

        //____________________________________________________________TASK9_____________________________________________________________________________
        System.out.println("----------TASK9----------");

        ArrayList<String> kitchen = new ArrayList<>(Arrays.asList("Plate","spoon","fork","Knife","cup","Mixer"));

        System.out.println(kitchen);

        int upper = 0;
        int lower = 0;
        int hasP = 0;
        int startP =0;

        for (String s : kitchen) {
            if (Character.isUpperCase(s.charAt(0))) upper++;
            else lower++;
            if (s.toLowerCase().contains("p")){
                hasP++;
                if (s.toLowerCase().startsWith("p") || s.toLowerCase().endsWith("p")) startP++;
            }
        }
        System.out.println("Element starts with uppercase = " + upper);
        System.out.println("Element starts with lowercase = " + lower);
        System.out.println("Element having P or p = " + hasP);
        System.out.println("Element starting or ending with p = " + startP);

        //____________________________________________________________TASK10_____________________________________________________________________________
        System.out.println("----------TASK10----------");

        numbers = new ArrayList<>(Arrays.asList(3,5,7,10,0,20,17,10,23,56,78));

        System.out.println(numbers);

        int div10 = 0;
        int evenGreater15 = 0;
        int oddLess20 = 0;
        int less15Greater50 = 0;

        for (int number : numbers) {
            if (number % 10 == 0) div10++;
            if (number % 2 == 0 && number > 15) evenGreater15++;
            if (number % 2 == 1 && number < 20) oddLess20++;
            if (number < 15 || number > 50)less15Greater50++;
        }

        System.out.println("Elements that can be divided by 10 = " + div10);
        System.out.println("Elements that are even and greater than 15 = " + evenGreater15);
        System.out.println("Elements that are odd and less than 20 = " + oddLess20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + less15Greater50);

    }

}


