package homeworks;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Homeworks11 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(noSpace("     Hello     "));
        System.out.println(noSpace("Tech Global"));
        System.out.println(noSpace(" Hello World   "));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(replaceFirstLast("    A       "));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));
        System.out.println(replaceFirstLast(""));
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        System.out.println(hasVowel("java"));
        System.out.println(hasVowel("abc"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel(""));
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(checkAge(2010));
        System.out.println(checkAge(2006));
        System.out.println(checkAge(2050));
        System.out.println(checkAge(1920));
        System.out.println(checkAge(1800));
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(averageOfEdges(0,0,6));
        System.out.println(averageOfEdges(-2,-2,10));
        System.out.println(averageOfEdges(-3,15,-3));
        System.out.println(averageOfEdges(10,13,20));
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        System.out.println(Arrays.toString(noA(new String[]{"java", "hello", "123", "xyz"})));
        System.out.println(Arrays.toString(noA(new String[]{"appium", "123", "ABC", "java"})));
        System.out.println(Arrays.toString(noA(new String[]{"apple", "appium", "ABC","Alex", "A"})));
        //________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        System.out.println(Arrays.toString(no3or5(new int[]{7, 4, 11, 23, 17})));
        System.out.println(Arrays.toString(no3or5(new int[]{3, 4, 5, 6})));
        System.out.println(Arrays.toString(no3or5(new int[]{10, 11, 12, 13, 14, 15})));
        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        System.out.println(countPrimes(new int[]{-10, -3, 0, 1}));
        System.out.println(countPrimes(new int[]{7, 4, 11, 23, 17}));
        System.out.println(countPrimes(new int[]{41, 53, 19, 47, 67}));


    }

    public static String noSpace(String str){

        return str.replaceAll(" ","");
    }

    public static String replaceFirstLast(String str){

        if (str.trim().length() < 2) return "";
        else return str.charAt(str.length()-1) + str.substring(1,str.length()-1) + str.charAt(0);
    }

    public static boolean hasVowel(String str){

        return str.toLowerCase().contains("a") ||
                str.toLowerCase().contains("e")||
                str.toLowerCase().contains("i") ||
                str.toLowerCase().contains("o") ||
                str.toLowerCase().contains("u");
    }

    public static String checkAge(int year){

        int age = Year.now().getValue() - year;

        if (age > 100 || age < 0) return("AGE IS NOT VALID");
        else if (age <=16) return("AGE IS NOT ALLOWED");
        return("AGE IS ALLOWED");

    }

    public static int averageOfEdges(int num1, int num2, int num3){

        return (Math.max(Math.max(num1,num2),num3) + Math.min(Math.min(num1,num2),num3))/2;



    }

    public static String[] noA(String[] arr){

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().startsWith("a"))
                arr[i]="###";

        }

        return arr;


    }

    public static int[] no3or5(int[]arr){

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 15 == 0) arr[i] = 101;
            else if (arr[i] % 5 == 0) arr[i] = 99;
            else if (arr[i] % 3 == 0) arr[i] = 100;

        }
        return arr;
    }

    public static int countPrimes(int[] arr){

        int count = 0;
        for (int num : arr) {
            boolean isPrime = true;
            if (num <= 1) {
                isPrime = false;
            }
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                count++;
            }
        }
        return count;
    }







}
