package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homeworks09 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        findFirstDuplicate(new int[]{-8, 56, 7, 8, 65});

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        findFirstDuplicateSting(new String[]{"Z","abc","z","123","#"});
        findFirstDuplicateSting(new String[]{"xyz","java","abc"});
        findFirstDuplicateSting(new String[]{"a","b","B","XYZ","123"});


        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        findAllDuplicates(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0});
        findAllDuplicates(new int[]{1, 2, 5, 0, 7});

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        findAllDuplicateStrings(new String[]{"A","foo","12","Foo","bar","a","a","java"});

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        System.out.println(Arrays.toString(reverseArray(new String[]{"abc","foo","bar"})));
        System.out.println(Arrays.toString(reverseArray(new String[]{"java","python","ruby"})));

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        System.out.println(reverseString("Today is a dun day"));

        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");

        System.out.println(dupe(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0}));
        System.out.println(dupe(new int[]{1, 2, 5, 0, 7}));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");
        System.out.println(dupeString(new String[]{"A","foo","12","Foo","bar","a","a","java"}));
    }

    public static void findFirstDuplicate(int[] num){

        String empty = "";

        for (int i = 0; i < num.length; i++) {
            for (int j = i+1; j < num.length ; j++) {
                if (empty.length()==0 && num[i] == num[j]) {
                    System.out.println(num[i]);
                    empty += num[i];
                    break;
                }
            }
        }
        if (empty.length()== 0) System.out.println("There is no duplicates");


    }

    public static void findFirstDuplicateSting(String[] arr){
        String empty = "";

        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length ; j++) {
                if (empty.length()==0 && arr[i].equalsIgnoreCase(arr[j])) {
                    System.out.println(arr[i]);
                    empty += arr[i];
                    break;
                }
            }
        }
        if (empty.length()== 0) System.out.println("There is no duplicates");





    }

    public static void findAllDuplicates(int[] num){

        String empty = "";

        for (int i = 0; i < num.length; i++) {
            for (int j = i+1; j < num.length ; j++) {
                if (!empty.contains(""+num[i])&&num[i] == num[j]) {
                    System.out.println(num[i]);
                    empty += num[i];
                    break;
                }
            }
        }
        if (empty.length()== 0) System.out.println("There is no duplicates");




    }

    public static void findAllDuplicateStrings(String[] arr){

        String empty = "";

        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length ; j++) {
                if (!empty.toLowerCase().contains(""+arr[i].toLowerCase())&& arr[i].equalsIgnoreCase(arr[j])) {
                    System.out.println(arr[i]);
                    empty += arr[i];
                    break;
                }
            }
        }
        if (empty.length()== 0) System.out.println("There is no duplicates");


    }

    public static String[] reverseArray(String[] arr){

        String[] reverse = new String[arr.length];

        int z = 0;

//        "abc","foo","bar"
        //  0     1      2
        for (int i = arr.length -1; i >= 0; i--) {
            reverse[z++] = arr[i];
        }
        return reverse;



    }

    public static String reverseString(String str){

        String reverse = "";

        //Java is fun
        //avaJ si nuf

        for(String e : str.split(" ")){
            for (int i = e.length()-1; i >=0; i--) {
                reverse += e.charAt(i);

            }
            reverse += " ";

        }

        return reverse;
    }

    public static ArrayList<Integer> dupe(int[] arr){

        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i <= arr.length-1; i++) {
            for (int j = i+1; j <= arr.length-1; j++) {
                if (arr[i] == arr[j]&& !(list.contains(arr[i]))) list.add(arr[i]);

            }



        }

        return list;
    }

    public static ArrayList<String> dupeString(String[] arr){

        ArrayList<String> list = new ArrayList<>();
        String empty = "";

        for (int i = 0; i <= arr.length-1; i++) {
            for (int j = i+1; j <= arr.length-1; j++) {
                if (arr[i].equalsIgnoreCase(arr[j]) && !(empty.toLowerCase().contains(arr[i].toLowerCase()))) empty += (arr[i] +" ");

            }



        }

        System.out.println(empty);

        Collections.addAll(list, empty.split(" "));


        return list;
    }

}
