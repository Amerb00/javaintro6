package homeworks;

public class Homeworks17 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(nthWord("I like programming languages",2));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        System.out.println(isArmStrong(153));
        System.out.println(isArmStrong(123));
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(reverseNumber(371));
        System.out.println(reverseNumber(12));
        System.out.println(reverseNumber(654));
    }

    public static String nthWord(String str, int num){

        return str.split(" ")[num-1];

    }

    public static boolean isArmStrong(int num){

        String numStr = String.valueOf(num);
        int numDigits = numStr.length();

        int armstrongSum = 0;
        for (int i = 0; i < numDigits; i++) {
            int digit = Character.getNumericValue(numStr.charAt(i));
            armstrongSum += Math.pow(digit, numDigits);
        }

        return armstrongSum == num;
    }
    public static int reverseNumber(int num) {
        int reversed = 0;

        while (num != 0) {
            int digit = num % 10;
            reversed = reversed * 10 + digit;
            num /= 10;
        }

        return reversed;
    }
}
