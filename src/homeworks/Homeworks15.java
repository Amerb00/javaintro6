package homeworks;

import java.util.*;

public class Homeworks15 {
    public static void main(String[] args) {
        System.out.println("----------TASK1----------");

        System.out.println(Arrays.toString(fibonacciSeries1(3)));
        System.out.println(Arrays.toString(fibonacciSeries1(5)));
        System.out.println(Arrays.toString(fibonacciSeries1(7)));
        System.out.println(Arrays.toString(fibonacciSeries1(2)));
        System.out.println(Arrays.toString(fibonacciSeries1(1)));

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        System.out.println(fibonacciSeries2(2));
        System.out.println(fibonacciSeries2(4));
        System.out.println(fibonacciSeries2(8));



        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        System.out.println(Arrays.toString(findUniques(new int[]{},new int[]{})));
        System.out.println(Arrays.toString(findUniques(new int[]{},new int[]{1, 2, 3, 2})));
        System.out.println(Arrays.toString(findUniques(new int[]{1, 2, 3, 4},new int[]{3, 4, 5, 5})));
        System.out.println(Arrays.toString(findUniques(new int[]{8, 9},new int[]{9, 8, 9})));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        System.out.println(firstDuplicate(new int[]{}));
        System.out.println(firstDuplicate(new int[]{1}));
        System.out.println(firstDuplicate(new int[]{1,2,2,3}));
        System.out.println(firstDuplicate(new int[]{1,2,3,3,4,1}));


        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(isPowerOf3(1));
        System.out.println(isPowerOf3(2));
        System.out.println(isPowerOf3(3));
        System.out.println(isPowerOf3(81));
        System.out.println(isPowerOf3(9));
        System.out.println(isPowerOf3(27));
        System.out.println(isPowerOf3(6));




    }


    public static int[] fibonacciSeries1(int n){


        int[] fib = new int[n];

        int first = 0;
        int second = 1;
        int sum = 1;

        for (int i = 0; i < n ; i++) {
            fib[i] = first;
            sum = first + second;
            first = second;
            second = sum;
        }

        return fib;
    }

    public static int fibonacciSeries2(int n){


        int[] fib = new int[n];

        int first = 0;
        int second = 1;
        int sum = 1;

        for (int i = 0; i < n ; i++) {
            fib[i] = first;
            sum = first + second;
            first = second;
            second = sum;
        }

        return fib[n-1];
    }

    public static int[] findUniques(int[] arr1, int[] arr2){

        TreeSet<Integer> unique = new TreeSet<>();

        for (int k : arr1) {
            unique.add(k);

        }
        for (int j : arr2) {
            unique.add(j);
        }

        int[] arr = new int[unique.size()];
        int i = 0;
        for (int value : unique) {
            arr[i] = value;
            i++;
        }

        return arr;

    }

    public static int firstDuplicate(int[] arr){

        TreeMap<Integer,Integer> dupes = new TreeMap<>();

        for (int i = 0; i < arr.length; i++) {
            int index =0;
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i] == arr[j]){
                    dupes.put(index,arr[i]);
                    continue;
                }
                index++;
            }
        }

       if (dupes.isEmpty()) return -1;

       return dupes.get(dupes.firstKey());

    }

    public static boolean isPowerOf3(int num){

        while (num % 3 == 0) {
            num /= 3;
        }

        return num ==1;


    }
}
