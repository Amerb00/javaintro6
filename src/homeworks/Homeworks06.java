package homeworks;

import java.util.Arrays;

public class Homeworks06 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        int[] num = new int[10];

        num[2] = 23;
        num[4] = 12;
        num[7] = 34;
        num[9] = 7;
        num[6] = 15;
        num[0] = 89;

        System.out.println(num[3]);
        System.out.println(num[0]);
        System.out.println(num[9]);

        System.out.println(Arrays.toString(num));

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";


        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        int[] numbers = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        String[] cartoons = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoons));

        boolean hasP = false;

        for (String cartoon : cartoons) {
            if (cartoon.contains("Pluto")) {
                hasP = true;
                break;
            }
        }
        System.out.println(hasP);
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        String[] cartoons2 = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cartoons2);
        System.out.println(Arrays.toString(cartoons2));

        boolean hasGF = false;

        for (String s : cartoons2) {
            if (s.contains("Garfield") && s.contains("Felix")) {
                hasGF = true;
                break;
            }

        }
        System.out.println(hasGF);

        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");


        double[] d = {10.5, 20.75, 70.0, 80.0, 15.75};

        System.out.println(Arrays.toString(d));

        for (double v : d) {
            System.out.println(v);

        }
        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(chars));
        int countL =0;
        int countLower = 0;
        int countUpper = 0;
        int countDigit = 0;
        int countSpecial = 0;
        for (char aChar : chars) {
            if (!Character.isLetterOrDigit(aChar)) countSpecial ++;
            else if (Character.isLetter(aChar)) {
                countL++;
                if (Character.isLowerCase(aChar))
                    countLower++;
                else if (Character.isUpperCase(aChar)) {
                    countUpper++;
                }
            }
            else if (Character.isDigit(aChar)) countDigit++;
            }

        System.out.println("Letter = " +countL);
        System.out.println("Uppercase Letter = " + countUpper);
        System.out.println("Lowercase Letter = " +countLower);
        System.out.println("Digits = " +countDigit);
        System.out.println("Special characters = " +countSpecial);


        //____________________________________________________________TASK9_____________________________________________________________________________
        System.out.println("----------TASK9----------");

        String[] str2 = {"Pen", "notebook", "Book" , "paper" , "bag" , "pencil", "Ruler"};

        System.out.println(Arrays.toString(str2));
        int upper = 0;
        int lower = 0;
        int bP = 0;
        int bookPen =0;

        for (String s : str2) {
            if (Character.isUpperCase(s.charAt(0))) upper++;
            if (Character.isLowerCase(s.charAt(0))) lower++;
            if (s.toLowerCase().startsWith("b")|| s.toLowerCase().startsWith("p")) bP++;
            if (s.toLowerCase().contains("book")|| s.toLowerCase().contains("pen")) bookPen++;
        }

        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);
        System.out.println("Elements starting with B or P = " + bP);
        System.out.println("Elements having \"book\" or \"pen\" = " + bookPen);

        //____________________________________________________________TASK10_____________________________________________________________________________
        System.out.println("----------TASK10----------");


        int[] num2 ={3,5,7,10,0,20,17,10,23,56,78};
        System.out.println(Arrays.toString(num2));

        int countMore =0;
        int countLess =0;
        int exact =0;
        for (int i : num2) {
            if (i > 10) countMore++;
            else if (i < 10) countLess++;
            else if (i == 10) exact++;
        }

        System.out.println("Elements that are more than 10 = " + countMore);
        System.out.println("Elements that are less than 10 = " + countLess);
        System.out.println("Elements that are 10 = " + exact);

        //____________________________________________________________TASK11_____________________________________________________________________________
        System.out.println("----------TASK11----------");

        int[] one = {5,8,13,1,2};
        int[] two = {9,3,67,1,0};
        int[] three = {Math.max(one[0],two[0]),Math.max(one[1],two[1]),Math.max(one[2],two[2]),Math.max(one[3],two[3]),Math.max(one[4],two[4])};





        
        System.out.println(Arrays.toString(one));
        System.out.println(Arrays.toString(two));
        System.out.println(Arrays.toString(three));











    }


    }

