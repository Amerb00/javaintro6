package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homeworks13 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("123$"));
        System.out.println(hasLowerCase("hello"));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        System.out.println(noZero(new ArrayList<>(Arrays.asList(1))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1,1,10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0,1,10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0,0,0))));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 2, 3})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{0, 3, 6})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 4})));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(containsValue(new String[]{"abc","foo","java"},"hello"));
        System.out.println(containsValue(new String[]{"abc","def","123"},"Abc"));
        System.out.println(containsValue(new String[]{"abc","def","123","Java","Hello"},"123"));

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(reverseSentence("Hello"));
        System.out.println(reverseSentence("Java is fun"));
        System.out.println(reverseSentence("This is a sentence"));

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypress"));


        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%Cypress",})));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java","is","fun")),new ArrayList<>(Arrays.asList("abc","xyz","123"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java","is","fun")),new ArrayList<>(Arrays.asList("Java","C#","Python"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java","C#","C#")),new ArrayList<>(Arrays.asList("Python","C#","C++"))));

        //____________________________________________________________TASK9_____________________________________________________________________________
        System.out.println("----------TASK9----------");
        System.out.println(noXinVariables(new ArrayList<>(Arrays.asList("abc","123","#$%"))));
        System.out.println(noXinVariables(new ArrayList<>(Arrays.asList("xyz","123","#$%"))));
        System.out.println(noXinVariables(new ArrayList<>(Arrays.asList("x","123","#$%"))));
        System.out.println(noXinVariables(new ArrayList<>(Arrays.asList("xyXyxy","Xx","ABC"))));



    }

    public static boolean hasLowerCase(String str){
        for (int i = 0; i <= str.length()-1 ; i++) {
            if (Character.isLowerCase(str.charAt(i))) return true;
        }
        return false;
    }
    public static ArrayList<Integer> noZero(ArrayList<Integer> list){

        ArrayList<Integer> empty = new ArrayList<>();

        for (Integer integer : list) {
            if (integer != 0) empty.add(integer);

        }
        return empty;

    }
    public static int[][] numberAndSquare(int[] arr){

        int[][] square = new int[arr.length][2];

        for (int i = 0; i < arr.length ; i++) {
            square[i][0] = arr[i];
            square[i][1] = arr[i] * arr[i];

        }
        return square;
        
    }
    public static boolean containsValue(String[] arr, String str){
        for (String s : arr) {
            if (s.equals(str)) return true;

        }
        return false;
    }
    public static String reverseSentence(String str){

        String[] words = str.split(" ");

        if (words.length >= 2){

            StringBuilder reversed = new StringBuilder();
            for (int i = words.length - 1; i >= 0; i--) {
                String word = words[i];
                reversed.append(word.toLowerCase()).append(" ");
            }

            String reversedSentence = reversed.toString().trim();
            return reversedSentence.substring(0, 1).toUpperCase() + reversedSentence.substring(1).toLowerCase();

        }
        return "There is not enough words!";

    }
    public static String removeStringSpecialsDigits(String str){

        return str.replaceAll("[^A-Za-z ]", "");

    }
    public static String[] removeArraySpecialsDigits(String[] arr){

        for (int i = 0; i < arr.length ; i++) {

            arr[i] = arr[i].replaceAll("[^A-Za-z ]", "");

        }

        return arr;
    }
    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1,ArrayList<String> list2){

        ArrayList<String> empty = new ArrayList<>();
        for (int i = 0; i <list1.size() ; i++) {
            if (list1.get(i).equals(list2.get(i))) empty.add(list1.get(i));

        }
        return empty;
    }
    public static ArrayList<String> noXinVariables(ArrayList<String> list){

        ArrayList<String> empty = new ArrayList<>();

        for (String s : list) {
           if (!s.contains("x") || !s.contains("X")) {
                empty.add(s.replaceAll("[xX]", ""));
            }
        }
        return empty;

    }
    
}
