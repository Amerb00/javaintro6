package homeworks;

import utilities.ScannerHelper;

public class Homeworks05 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        for (int i = 1; i <= 100; i++) {

            if( i == 98) System.out.println(i);
            else if( i % 7 == 0) System.out.print(i + "-");

        }

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("\n----------TASK2----------\n");

        for (int i = 1; i <= 50 ; i++) {

            if(i % 6 == 0) System.out.print(i + "-");
            else if( i == 50) System.out.println(i);
        }
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("\n----------TASK3----------\n");

        for (int i = 100; i >= 50 ; i--) {
            if( i == 50) System.out.println(i);
            else if (i % 5 == 0) System.out.print(i + "-");
        }
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("\n----------TASK4----------\n");

        for (int i = 0; i <= 7; i++) {
            System.out.println("The square of " + i + " is = " + i*i);
        }
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("\n----------TASK5----------\n");

        int sum = 0;

        for (int i = 1; i <= 10 ; i++) {
            sum += i;
        }
        System.out.println(sum);
//____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("\n----------TASK6----------\n");

        int num = ScannerHelper.getPositiveNumber();
        int fac = 1;

        for (int i = 1; i <= num ; i++) {
        fac *=i;
        }
        System.out.println(fac);

        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("\n----------TASK7----------\n");

        String name = ScannerHelper.getFullName().toLowerCase();
        int count = 0;

        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == 'a'||name.charAt(i) == 'e'||name.charAt(i) == 'i'||name.charAt(i) == 'o'||name.charAt(i) == 'u')
                count++;
        }
        System.out.println("There are "+ count + " vowel letters in this full name");


        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("\n----------TASK8----------\n");
        String name1;
        do {
           name1 = ScannerHelper.getFullName().toLowerCase();
        }
        while (!name1.startsWith("j"));

        System.out.println("End of the program");



    }


}
