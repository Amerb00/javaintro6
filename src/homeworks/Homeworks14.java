package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homeworks14 {
    public static void main(String[] args) {
//____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        fizzBuzz1(3);
        fizzBuzz1(5);
        fizzBuzz1(18);

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        System.out.println(fizzBuzz2(0));
        System.out.println(fizzBuzz2(1));
        System.out.println(fizzBuzz2(3));
        System.out.println(fizzBuzz2(5));
        System.out.println(fizzBuzz2(15));


        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(findSumNumbers("abc$"));
        System.out.println(findSumNumbers("a1b4c 6#"));
        System.out.println(findSumNumbers("ab110c045d"));
        System.out.println(findSumNumbers("525"));


        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(findBiggestNumber("abc$"));
        System.out.println(findBiggestNumber("a1b4c 6#"));
        System.out.println(findBiggestNumber("ab110c045d"));
        System.out.println(findBiggestNumber("525"));


        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        System.out.println(countSequenceOfCharacters(""));
        System.out.println(countSequenceOfCharacters("abc"));
        System.out.println(countSequenceOfCharacters("aabbcca"));
        System.out.println(countSequenceOfCharacters("aaAAa"));



    }

    public static void fizzBuzz1(int num){

        for (int i = 1; i <= num ; i++) {

            if (i % 3 == 0 && i % 5 ==0) System.out.println("FizzBuzz");
            else if (i % 3 == 0) System.out.println("Fizz");
            else if (i % 5 == 0) System.out.println("Buzz");
            else System.out.println(i);
        }
    }
    public static String fizzBuzz2(int num){


        if (num % 3 == 0 && num % 5 == 0) return "FizzBuzz";
        else if (num % 3 == 0) return "Fizz";
        else if (num % 5 == 0) return "Buzz";
        else return num +"";




    }
    public static int findSumNumbers(String str){

        int sum = 0;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (Character.isDigit(ch)) {
                currentNumber.append(ch);
            } else {
                if (currentNumber.length() > 0) {
                    sum += Integer.parseInt(currentNumber.toString());
                    currentNumber.setLength(0);
                }
            }
        }

        if (currentNumber.length() > 0) {
            sum += Integer.parseInt(currentNumber.toString());
        }

        return sum;
    }
    public static int findBiggestNumber(String str) {
        int biggestNumber = 0;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (Character.isDigit(ch)) {
                currentNumber.append(ch);
            } else {
                if (currentNumber.length() > 0) {
                    int current = Integer.parseInt(currentNumber.toString());
                    biggestNumber = Math.max(biggestNumber, current);
                    currentNumber.setLength(0);
                }
            }
        }

        if (currentNumber.length() > 0) {
            int current = Integer.parseInt(currentNumber.toString());
            biggestNumber = Math.max(biggestNumber, current);
        }

        return biggestNumber;
    }
    public static String countSequenceOfCharacters(String str) {
        if (str.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        char prevChar = str.charAt(0);
        int count = 1;

        for (int i = 1; i < str.length(); i++) {
            char currentChar = str.charAt(i);

            if (currentChar == prevChar) {
                count++;
            } else {
                result.append(prevChar).append(count);
                prevChar = currentChar;
                count = 1;
            }
        }

        result.append(prevChar).append(count);

        return result.toString();
    }







}


