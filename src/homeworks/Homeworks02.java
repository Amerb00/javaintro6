package homeworks;

import java.util.Scanner;

public class Homeworks02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        //Assigning
        int num1, num2, num3, num4, num5, sum;

        //Asking user to input first number
        System.out.println("Enter first number.");
        num1 = input.nextInt();

        //Asking user to input second number
        System.out.println("Enter second number.");
        num2 = input.nextInt();

        //Printing the numbers the user entered
        System.out.println("The number 1 entered by user is = " + num1);
        System.out.println("The number 2 entered by user is = " + num2);

        //Found the sum and printed it out
        sum = num1 + num2;
        System.out.println("The sum of number 1 and 2 entered by user is = " + sum);

//        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");


        //Asking user to input first number
        System.out.println("Enter first number.");
        num1 = input.nextInt();

        //Asking user to input second number
        System.out.println("Enter second number.");
        num2 = input.nextInt();

        //Multiplied the 2 given numbers and printed
        System.out.println("The product of the given 2 numbers is: " + (num1 * num2));

//        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        double d1, d2;

        //Asking user to input first number
        System.out.println("Enter first number.");
        d1 = input.nextDouble();

        //Asking user to input second number
        System.out.println("Enter second number.");
        d2 = input.nextDouble();


        System.out.println("The sum of the given number is = " + (d1 + d2));        // The sum
        System.out.println("The product of the given number is = " + (d1 * d2));    // The product
        System.out.println("The subtraction of the given number is = " + (d1 - d2));// subtraction
        System.out.println("The division of the given number is = " + (d1 / d2));   // division
        System.out.println("The remainder of the given number is = " + (d1 % d2));  // remainder

//        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        System.out.println("1.     " + (-10 + 7 * 5));
        System.out.println("2.     " + (72 + 24) % 24);
        System.out.println("3.     " + (10 + -3 * 9 / 9));
        System.out.println("4.     " + (5 + 18 / 3 * 3 - (6 % 3)));

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");


        // Asking user to input first number
        System.out.println("Enter first number.");
        num1 = input.nextInt();

        //Asking user to input second number
        System.out.println("Enter second number.");
        num2 = input.nextInt();

        // Finding average
        System.out.println("The average of the given numbers is: " + ((num1 + num2) / 2));


        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        // Asking user to input first number
        System.out.println("Enter first number.");
        num1 = input.nextInt();

        //Asking user to input second number
        System.out.println("Enter second number.");
        num2 = input.nextInt();

        //Asking user to input third number
        System.out.println("Enter third number.");
        num3 = input.nextInt();

        //Asking user to input fourth number
        System.out.println("Enter fourth number.");
        num4 = input.nextInt();

        //Asking user to input fifth number
        System.out.println("Enter fifth number.");
        num5 = input.nextInt();

        // Finding average
        System.out.println("The average of the given numbers is: " + ((num1 + num2 + num3 + num4 + num5) / 5));


        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");

        // Asking user to input first number
        System.out.println("Enter first number.");
        num1 = input.nextInt();

        //Asking user to input second number
        System.out.println("Enter second number.");
        num2 = input.nextInt();

        //Asking user to input third number
        System.out.println("Enter third number.");
        num3 = input.nextInt();

        //Finding square of users input
        System.out.println("The " + num1 + " multiplied with " + num1 + " is = " + (num1 * num1));
        System.out.println("The " + num2 + " multiplied with " + num2 + " is = " + (num2 * num2));
        System.out.println("The " + num3 + " multiplied with " + num3 + " is = " + (num3 * num3));


        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");

        // Asking user to input first number
        System.out.println("Enter the side of a square.");
        num1 = input.nextInt();
        input.nextLine();

        //finding perimeter and area of users input
        System.out.println("Perimeter of the square = " + (4 * num1));
        System.out.println("Area of the square = " + (num1 * num1));


        //____________________________________________________________TASK9_____________________________________________________________________________
        System.out.println("----------TASK9----------");


        double salary = 90_000;
        salary *= 3;

        System.out.println("A Software Engineer in Test can earn $" + salary + " in 3 years");


        //____________________________________________________________TASK10_____________________________________________________________________________
        System.out.println("----------TASK10----------");

        System.out.println("Whats your favorite book?");
        String book = input.nextLine();

        System.out.println("Whats your favorite color?");
        String color = input.nextLine();

        System.out.println("Whats your favorite number?");
        int num = input.nextInt();

        System.out.println("User's favorite book is: " + book + "" +
                "\nUser's favorite color is: " + color + "" +
                "\nUser's favorite number is: " + num);


        //____________________________________________________________TASK11_____________________________________________________________________________
        System.out.println("----------TASK11----------");

        String fName, lName, email, phoneNum, address;
        int age;

        //Users first name
        System.out.println("Enter first name");
        fName = input.next();

        //Users last name
        System.out.println("Enter last name");
        lName = input.next();

        //Users age
        System.out.println("Enter age");
        age = input.nextInt();

        //Users email address
        System.out.println("Enter email address");
        email = input.next();
        input.nextLine();

        //Users Phone number
        System.out.println("Enter phone number");
        phoneNum = input.nextLine();

        //Users address
        System.out.println("Enter address");
        address = input.nextLine();

        System.out.println("\t User who joined this program is " + fName + " " + lName + ". " + fName + "'s age is " + age + ". " + fName + "'s email \n" +
                "address is " + email + ", phone number is " + phoneNum + ", and address \n" +
                "is " + address + ".");


    }
}
