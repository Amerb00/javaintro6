package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homeworks08 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(countConsonats("JAVA IS FUN"));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you")));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(removeExtraSpaces("Java is   fun"));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        System.out.println(count3OrLess());

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(isDatFormatValid("01-12-1999"));

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        System.out.println(isEmailFormatValid("db@@gmail"));

    }


    public static int countConsonats(String str){

        return str.replaceAll("[aeiouAEIOU ]","").length();
    }

    public static String[] wordArray(String str){

        return str.split("\\s+");
    }

    public static String removeExtraSpaces(String str){
        return str.replaceAll("\\s+", " ").trim();
    }

    public static int count3OrLess(){

        String str = ScannerHelper.getString();

        Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
        Matcher matcher = pattern.matcher(str);
        int count =0;
        while (matcher.find()){
            count++;
        }
        return count;
    }

    public static boolean isDatFormatValid(String str){

        return str.matches("\\d{2}/\\d{2}/\\d{4}");

    }

    public static boolean isEmailFormatValid(String str){

        return str.matches("\\w{2,}@[a-zA-z]{2,}.[a-zA-z]{2,}");


    }



}
