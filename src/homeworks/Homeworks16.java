package homeworks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Homeworks16 {

    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(parseDate("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London"));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        HashMap<String, Integer> items = new HashMap<String, Integer>();
        items.put("Apple",2);
        items.put("Pineapple",1);
        items.put("Orange",3);


        System.out.println(calculateTotalPrice1(items));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        HashMap<String, Integer> items2 = new HashMap<String, Integer>();
        items2.put("Apple",4);
        items2.put("Mango",8);
        items2.put("Orange",3);


        System.out.println(calculateTotalPrice2(items2));


    }


    public static HashMap<String,String> parseDate(String str){

        HashMap<String,String> date = new HashMap<>();

        String[] num = str.split("[^0-9]+");
        String[] place = str.split("[^A-z]+");

        for (int i = 0; i < num.length ; i++) {
            date.put(num[i],place[i]);

        }

        return date;

    }

    public static double calculateTotalPrice1(HashMap<String,Integer> cart){


        double apple = 2.00;
        double orange = 3.29;
        double mango = 4.99;
        double pineapple = 5.25;

        double totalPrice = 0;

        for (HashMap.Entry<String, Integer> entry : cart.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            double price = 0.0;
            switch (item) {
                case "Apple":
                    price = apple;
                    break;
                case "Orange":
                    price = orange;
                    break;
                case "Mango":
                    price = mango;
                    break;
                case "Pineapple":
                    price = pineapple;
                    break;
                default:
                    System.out.println("Invalid item: " + item);
            }

            double itemTotal = price * amount;
            totalPrice += itemTotal;
        }


        return totalPrice;




    }

    public static double calculateTotalPrice2(HashMap<String,Integer> cart){

        double apple = 2.00;
        double orange = 3.29;
        double mango = 4.99;

        double totalPrice = 0;

        int appleCount =0;
        int mangoCount =0;

        for (HashMap.Entry<String, Integer> entry : cart.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            double price = 0.0;
            switch (item) {
                case "Apple":
                    price = apple;
                    appleCount += amount;
                    break;
                case "Orange":
                    price = orange;
                    break;
                case "Mango":
                    price = mango;
                    mangoCount += amount;
                    break;
                default:
                    System.out.println("Invalid item: " + item);
            }

            double itemTotal = price * amount;
            totalPrice += itemTotal;
        }

        int discountedApples = appleCount / 2;
        double appleDiscount = discountedApples * (apple / 2.0);
        totalPrice -= appleDiscount;

        int freeMangoes = mangoCount / 4;
        double mangoDiscount = freeMangoes * mango;
        totalPrice -= mangoDiscount;


        return totalPrice;







    }




}
