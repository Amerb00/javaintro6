package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homeworks10 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        System.out.println(countWords("      Java is fun       "));


        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");


        System.out.println(countA("QA stands for Quality Assurance"));



        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));



        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));



        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java","C#","ruby","JAVA","ruby","C#","C++"))));


        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));
        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        System.out.println(Arrays.toString(add(new int[]{3, 0, 0, 7, 5, 10}, new int[]{6, 3, 2})));
        System.out.println(Arrays.toString(add(new int[]{10, 3, 6, 3, 2}, new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));
        System.out.println(Arrays.toString(add(new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34}, new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));

        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");
        System.out.println(findClosestTo10(new int[] {10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[] {10, -13, 8, 12, 15, -20}));


    }

    public static int countWords(String str){
        return str.trim().split(" ").length;
    }

    public static int countA(String str){

        int count = 0;
        for(char c : str.toCharArray()){
            if (c == 'a' || c == 'A') count++;
        }

        return count;
    }

    public static int countPos(ArrayList<Integer> list){

        list.removeIf(element -> element % 2 == 1 || element % 2 == -1);

        return list.size();

    }

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> list){

        ArrayList<Integer> empty = new ArrayList<>();

        for (Integer integer : list) {
            if (!empty.contains(integer)) empty.add(integer);

        }
        return empty;

    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list){

        ArrayList<String> empty = new ArrayList<>();

        for (String s : list) {
            if(!empty.contains(s))empty.add(s);

        }
        return empty;
    }

    public static String removeExtraSpaces(String str){


        String e = "";


        for(String s :str.trim().split("\\s+")) {

            e += s + " ";

        }
        return e;

    }

    public static int[] add(int[]arr1,int[]arr2) {

        int max = Math.max(arr2.length, arr1.length);
        int min = Math.min(arr2.length, arr1.length);

        /*
        {3, 0, 0, 7, 5, 10};
         +  +  +
        {6, 3, 2};
        [9, 3, 2, 7, 5, 10]
         */

        int[] sum = new int[max];

        for (int i = 0; i < max; i++) {
            if (i < min ) sum[i] = arr1[i] + arr2[i];
            else if (arr1.length > arr2.length)sum[i] = arr1[i];
            else sum[i] = arr2[i];


        }
        return sum;

    }

    public static int findClosestTo10(int[] arr){


        int closest = Integer.MAX_VALUE;
        int diff = Integer.MAX_VALUE;

        for (int num : arr) {
            if (num != 10 && Math.abs(10 - num) < diff) {
                diff = Math.abs(10 - num);
                closest = num;
            }
        }

        return closest;


    }
}
