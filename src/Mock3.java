import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Mock3 {

    public static void main(String[] args) {
        System.out.println(extracted("ab3c7d"));
        System.out.println(extracted("asdh3dn1"));
        System.out.println(extracted("ab2c7d3"));
        System.out.println(extracted("afdf3fe3df3"));
        System.out.println(extracted("b110c045d"));

    }

    private static int extracted(String str) {
        String num = str.replaceAll("[^1-9]","");

        int sum = 0;
        for (int i = 0; i < num.length(); i++) {
            sum += Character.getNumericValue(num.charAt(i));
        }
        return sum;


    }

    public Map<String, String> mapBully(Map<String, String> map) {

        if(map.containsKey("a")) {
            map.put("b", map.get("a"));
            map.put("a","");
        }
        return map;


    }
}
