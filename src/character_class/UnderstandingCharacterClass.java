package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {


        String str = ScannerHelper.getString();

        System.out.println(str.charAt(0) >= 41 && str.charAt(0) <= 90);

    }
}
