package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();

        int countL =0;
        int countD =0;

        for (int i = 0; i < str.length() ; i++) {
            if (Character.isLetter(str.charAt(i))) countL++;
            if (Character.isDigit(str.charAt(i))) countD++;
        }

        System.out.println("This string has " + countD + " digits and " + countL + " letters");

        System.out.println("--------------Task 2------------");


        String str1 = ScannerHelper.getString();

        int upper =0;
        int lower =0;

        for (int i = 0; i < str.length() ; i++) {
            if (Character.isUpperCase(str.charAt(i))) upper++;
            if (Character.isLowerCase(str.charAt(i))) lower++;
        }

        System.out.println("This string has " + upper + " uppercase letters and " + lower + " lowercase letters");


        System.out.println("--------------Task 3------------");

        String str2 = ScannerHelper.getString();

        int count =0;

        for (int i = 0; i < str2.length(); i++) {
            if (!Character.isLetterOrDigit(str2.charAt(i)) && !Character.isWhitespace(str2.charAt(i))) count++;

        }
        System.out.println(count);

        System.out.println("--------------Task 4------------");

        String str4 = ScannerHelper.getString();

        int countLetter =0;
        int countDigit =0;
        int countUpper = 0;
        int countLower =0;
        int countSpace = 0;
        int special = 0;

        for (int i = 0; i < str4.length() ; i++) {
            if (Character.isLetter(str4.charAt(i))) countLetter++;
            else if (Character.isDigit(str4.charAt(i))) countDigit++;
            else if (Character.isWhitespace(str4.charAt(i))) countSpace++;
            else if (Character.isUpperCase(str4.charAt(i))) countUpper++;
            else if (Character.isLowerCase(str4.charAt(i))) countLower++;
            else if (!Character.isLetterOrDigit(str4.charAt(i)) && !Character.isWhitespace(str4.charAt(i))) count++;

        }
        System.out.println("letters = " + countLetter);
        System.out.println("Uppercase letters = " + countUpper);
        System.out.println("Lowercase letters = " + countLower);
        System.out.println("Digits = " + countDigit);
        System.out.println("Spaces = " + countSpace);
        System.out.println("Specials = " + special);



    }
}
