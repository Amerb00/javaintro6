package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _03_Additional_Methods {
    public static void main(String[] args) {

        ArrayList<String> group1Members = new ArrayList<>();
        group1Members.add("Belal");
        group1Members.add("Assem");
        group1Members.add("Gurkan");
        group1Members.add("Dima");


        ArrayList<String> group2Members = new ArrayList<>();
        group2Members.add("Adam");
        group2Members.add("Melek");
        group2Members.add("Cihan");


        ArrayList<String> group3Members = new ArrayList<>();
        group3Members.add("Yousef");
        group3Members.add("Sandina");

        System.out.println("Group 1 members  = " + group1Members);
        System.out.println("Group 2 members  = " + group2Members);
        System.out.println("Group 3 members  = " + group3Members);


        ArrayList<String> allMembers = new ArrayList<>();

        allMembers.addAll(group2Members);
        allMembers.addAll(group1Members);
        allMembers.addAll(group3Members);

        System.out.println(allMembers);

        allMembers.removeIf(name -> !name.equals("Gurkan"));
        allMembers.removeIf(name -> !name.equals("Cihan"));






    }
}
