package arraylist_linkedlisted_vector;

import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList_vector {
    public static void main(String[] args) {

        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin","Rome", "Kyiv","Ankara","Madrid", "Chicago"));

        System.out.println(cities.size());
        System.out.println(cities.contains("Miami"));
        System.out.println(cities.indexOf("Evanston"));

        System.out.println(cities.getFirst());// Berlin
        System.out.println(cities.getLast()); // Chicago

        System.out.println(cities.removeFirst()); // Remove Berlin
        System.out.println(cities.removeLast()); // Remove Chicago

        System.out.println(cities);
        cities.push("Barcelona");
        System.out.println(cities);

    }
}
