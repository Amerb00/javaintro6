package arraylist_linkedlisted_vector;

import java.util.ArrayList;

public class _02_Additional_Methods {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(15);
        list.add(20);
        list.add(10);
        list.add(20);
        list.add(30);

        System.out.println(list);
        System.out.println(list.size());


        System.out.println(list.indexOf(10));
        System.out.println(list.indexOf(20));
        System.out.println(list.lastIndexOf(10));
        System.out.println(list.lastIndexOf(20));

        list.remove((Integer) 15);
        list.remove((Integer) 30);
        list.remove((Integer) 302);
        System.out.println(list);
        list.remove((Integer) 10);

        list.removeIf(element -> element == 20);

        System.out.println(list);

        list.add(11);
        list.add(12);
        list.add(13);

        System.out.println(list);

        list.clear();
        System.out.println(list);






    }
}
