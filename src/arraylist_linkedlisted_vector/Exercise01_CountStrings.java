package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_CountStrings {
    public static void main(String[] args) {
        System.out.println("\n======task1=======");
        countO(new ArrayList<>(Arrays.asList("Hello","Hi","School","Computer")));
        System.out.println("\n======task2=======");
        System.out.println(more3(new ArrayList<>(Arrays.asList("Hello","Hi","School","Computer"))));
        System.out.println(more3(new ArrayList<>(Arrays.asList("abc","xyz"))));
        System.out.println(more3(new ArrayList<>(Arrays.asList("Laptop","Object"))));
        System.out.println("\n======task3=======");
    }


    public static void countO(ArrayList<String> array){

        int count = 0;
        for (String s : array) {
            if (s.toLowerCase().contains("o")) count++;
        }
        System.out.println(count);
    }
    public static int more3(ArrayList<String> list){

        int count = 0;
        for (String s : list) {
            if (s.length() >= 3) count++;
        }
        return count;
    }




}
