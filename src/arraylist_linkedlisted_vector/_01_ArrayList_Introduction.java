package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_ArrayList_Introduction {
    public static void main(String[] args) {

        //How to create an Array vs ArrayList
        String[] array = new String[3];
        ArrayList<String> list = new ArrayList<>();// capacity = 10 by default

        // How to get size of an Array and ArrayList
        System.out.println("\n-----TASK-2------");
        System.out.println("The size of the array = " + array.length);// 3
        System.out.println("The size of the list = " + list.size());// 0

        // How to print an Array vs Arraylist
        System.out.println("\n-----TASK-3------");
        System.out.println("The array = " + Arrays.toString(array));//[null, null ,null]
        System.out.println("The arraylist = " + list);// []

        //How to add elements to anArray vs Arraylist
        System.out.println("\n-----TASK-4------");
        array[0] = "John";
        array[1] = "Alex";
        array[2] = "Max";
        System.out.println("The array = " + Arrays.toString(array));

        list.add("Joe");
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");

        list.add(2,"Jazzy");
        System.out.println("The arraylist = " + list);//[Joe, Jane, Jazzy, Mike, Adam]

        //How to update an existing element in an Array vs ArrayList
        System.out.println("\n-----TASK-4------");
        array[0] = "Ali";
        System.out.println("The array = " + Arrays.toString(array));// [Ali, Alex, Max]

        list.set(1,"Jasmine");
        System.out.println("The arraylist = " + list);

        // How to loop an array vs arraylist
        System.out.println("\n-----TASK-5 fori loop------");

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("\n-----TASK-5 for each loop------");

        for (String value : array) {
            System.out.println(value);
        }

        for (String s : list) {
            System.out.println(s);
        }

    }
}
