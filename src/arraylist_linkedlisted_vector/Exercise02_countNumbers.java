package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise02_countNumbers {
    public static void main(String[] args) {


        System.out.println("\n======task1=======");
        System.out.println(countEven(new ArrayList<>(Arrays.asList(2,3,5))));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(10,20,30))));
        System.out.println(countEven(new ArrayList<>()));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(-1,3,17,25))));
        System.out.println("\n======Task2=======");

        more15(new ArrayList<>(Arrays.asList(-1,3,17,25)));
        System.out.println("\n======Task3=======");
        System.out.println(no3(new ArrayList<>(Arrays.asList(13,3,30,300,533))));




    }


    public static int countEven(ArrayList<Integer> list){

        //return (int) list.stream().filter(element -> element % 2 == 0).count();

        int count = 0;
        for (Integer integer : list) {
            if (integer % 2 == 0) count++;
        }
        return count;
    }

    public static void more15(ArrayList<Integer> list){

        System.out.println((int) list.stream().filter(element -> element > 15).count());

    }

    public static int no3(ArrayList<Integer> list){

        int count = 0;
        for (Integer integer : list) {
            if (integer % 10 == 3) count++;
        }
        return count;
    }






}
