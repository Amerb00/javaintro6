package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Array_to_ArrayList {
    public static void main(String[] args) {
        System.out.println("\n---------Way 1 asList() method---------");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Parris", "Rome"));
        System.out.println(cities);

        System.out.println("\n---------Way 2 loops---------");
        String[] countries = {"USA", "Germany","Spain", "Italy"};

        ArrayList<String> list = new ArrayList<>();

        for (String country : countries) {
            list.add(country);
        }

        System.out.println(list);
        System.out.println("\n---------Way 3 Collections.addAll() method---------");
        list = new ArrayList<>();
        Collections.addAll(list,countries);

        System.out.println(list);


    }
}
