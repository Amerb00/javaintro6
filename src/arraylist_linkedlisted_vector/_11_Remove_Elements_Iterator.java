package arraylist_linkedlisted_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class _11_Remove_Elements_Iterator {
    public static void main(String[] args) {

        System.out.println(removeVowels3(new ArrayList<>(Arrays.asList("Hello", "world", "xyz"))));
    }

    public static ArrayList<String> removeVowels3(ArrayList<String> list) {

        Iterator<String> iterator = list.iterator();


        while(iterator.hasNext()) {
            String currentElement = iterator.next();
            if (currentElement.toLowerCase().contains("a") ||
                    currentElement.toLowerCase().contains("e") || currentElement.toLowerCase().contains("i") ||
                    currentElement.toLowerCase().contains("o") || currentElement.toLowerCase().contains("u")) iterator.remove();
        }
        return list;
    }


}
