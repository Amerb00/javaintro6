package arraylist_linkedlisted_vector;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06 {
    public static void main(String[] args) {
        System.out.println("\n========TASK01=========\n");
        int[] arr = {3, 2, 5, 7, 0};
        System.out.println(Arrays.toString(double1(arr)));

        System.out.println("\n========TASK2=========\n");

        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1,7,1))));
        System.out.println("\n========TASK3=========\n");
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2,2,5,7,10,10))));
        System.out.println("\n========TASK4=========\n");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global","",null,"","School"))));
        System.out.println("\n========TASK5=========\n");
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200,5,100,99,101,75,-123))));
        System.out.println("\n========TASK6=========\n");
        System.out.println(uniquesWords("Global School"));




    }

    public static int[] double1(int[] num) {


        for (int i = 0; i < num.length; i++) {
            num[i] *= 2;
        }

        return num;
    }
    public static int secondMax(ArrayList<Integer> list) {

        Collections.sort(list);

        //2,2,5,7,10,10

        for (int i = list.size() - 1; i > 0; i--) {
            if (list.get(i) < list.get(list.size() - 1)) return list.get(i);
        }

        return 0;

    }
    public static int secondMin(ArrayList<Integer> list) {

        Collections.sort(list);

        //2,2,5,7,10,10
        //1,1,1,1,1,1,1

        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) > list.get(0)) return list.get(i);

        }
        return 0;

    }
    public static ArrayList<String> removeEmpty(ArrayList<String> list){

        ArrayList<String> empty = new ArrayList<>();

        for (String s : list) {
            if (!(s == null) && !s.isEmpty()) empty.add(s);
        }

        //list.removeIf(element -> element == null || element.isEmpty());

        return empty;

    }
    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list){

        list.removeIf(integer -> integer >= 100 || integer <= -100);
        return list;

    }

    public static ArrayList<String> uniquesWords(String str){

        ArrayList<String> stringArrayList = new ArrayList<>();

        for (String s : str.split(" ")) {
            if (!stringArrayList.contains(s)) stringArrayList.add(s);


        }

        return stringArrayList;

    }




}

