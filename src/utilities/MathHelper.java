package utilities;

import java.util.Random;

public class MathHelper {

    //-----------------Max-Min----------------------------
    public static int maxOf3(int a, int b, int c){
        return Math.max(Math.max( a,  b),  c);

    }
    public static int maxOf4(int a, int b, int c, int d){
        return Math.max(Math.max(Math.max( a,  b),  c),d);

    }
    //-----------------Sums----------------------------
    public static int sum(int a , int b){
        return (a + b);
    }
    public static double sum(double a , double b){
        return (a + b);
    }
    public static int sum(int a , int b , int c){
        return (a + b + c);
    }

    public static int product(int a , int b){
        return (a * b);
    }
    public static int square(int a){
        return a*a ;
    }
    public static boolean isOdd(int a){
        return a % 2 != 0;
    }
    public static boolean isEven(int a){
        return a % 2 == 0;
    }
    public static boolean isPositive(int a){
        return a > 0;
    }
    public static boolean isNegative(int a){
        return (a < 0);
    }
    public static boolean isNeutral(int a){
        return (a == 0) ;
    }
    public static boolean isMultipleOfSeven(int a){
        return (a % 7 == 0);
    }
    public static boolean isTen(int a){
        return (a == 10);
    }
    public static String firstLast(String a){

        if(a.length() >= 2) return (( "" +a.charAt(0) + a.charAt(a.length()-1)));
        return a;
    }



}
