package utilities;

import java.util.Scanner;

public class ScannerHelper {
   static Scanner input = new Scanner(System.in);

   public static String getFirstName(){
       System.out.println("Please enter a first name:");
        return input.nextLine();
    }
    public static String getLastName(){
        System.out.println("Please enter a last name:");
        return input.nextLine();
    }
    public static String getFullName(){
        System.out.println("Please enter your name:");
        return input.nextLine();
    }
    public static int getAge(){
        System.out.println("Please enter age:");
        int age = input.nextInt();
        input.nextLine();
        return age;
    }
    public static int getNumber(){
        System.out.println("Please enter number:");
        int num = input.nextInt();
        input.nextLine();
        return num;
    }
    public static int getPositiveNumber(){
        System.out.println("Please enter positive number:");
        int num = input.nextInt();
        input.nextLine();
        return num;
    }
    public static String doubleWord(String a){
       return a+a;
    }

    public static String getString(){
        System.out.println("Please enter a String");
        return input.nextLine();
    }
    public static String getAddress(){
        System.out.println("Please enter your address:");
        return input.nextLine();
    }
    public static String getFavCountry(){
        System.out.println("Please enter your favorite country:");
        return input.nextLine();
    }
    public static boolean startsVowel(String a){
        String b = a.toLowerCase();

        return (b.contains("a")|| b.contains("e") || b.contains("i") || b.contains("o") || b.contains("u"));
    }
    public String middleThree(String str) {
        if(str.length() > 4)return "" + str.charAt((str.length()-2) / 2) + str.charAt(str.length() / 2) + str.charAt((str.length()+ 2) / 2);

        return str;
    }
    public String seeColor(String str) {

        String red = "red";
        String blue = "blue";

        if(str.substring(0,3).equals(red)) return "red";
        return "";
    }
    public static String getMiddleEven(String str){

       return str.substring(str.length()/2 -1 , str.length()/2 +1 );
    }
    public static char getMiddleOdd(String str){

        return str.charAt(str.length()/2);
    }
}
