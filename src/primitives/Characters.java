package primitives;

public class Characters {
    public static void main(String[] args) {

        char c1 = 'A';
        char c2 = ' ';

        System.out.println(c1);

        char myFavCharacter = 'P';

        System.out.println("My favorite char = " + myFavCharacter);
    }
}
