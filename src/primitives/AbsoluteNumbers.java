package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {

        System.out.println("\n----------byte---------\n");
        byte myNumber = 45;
        System.out.println(myNumber);

        System.out.println("The max value of byte = " + Byte.MAX_VALUE);// 127
        System.out.println("The max value of byte = " + Byte.MIN_VALUE);// -128

        System.out.println("\n----------short---------\n");
        short numberShort = 150;
        System.out.println("numberShort");

        System.out.println("The max value of short = " + Short.MAX_VALUE);// 32767
        System.out.println("The min value of short = " + Short.MIN_VALUE); // - 32768

        System.out.println("\n----------int---------\n");
        int myInteger = 20000000;
        System.out.println(myInteger);
        System.out.println("The max value of int = " + Integer.MAX_VALUE); // 2147483647
        System.out.println("The min value of int = " + Integer.MIN_VALUE); // -2147483648

        System.out.println("\n----------int---------\n");
        long myBigNumber = 21474836478L;

        System.out.println(myBigNumber); //21474836478

        System.out.println("The max value of Long = " + Long.MAX_VALUE); // -2147483648
        System.out.println("The min value of Long = " + Long.MIN_VALUE); // -2147483648








    }
}
