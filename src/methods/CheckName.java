package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {
        /*
        I can invoke static methods with class name
        I can call non-static methods with object
        */



        String name =  ScannerHelper.getFirstName();

        String lName = ScannerHelper.getLastName();

        System.out.println("The full name entered is = " + name + " " + lName);





    }

}
