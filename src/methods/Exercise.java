package methods;

import utilities.MathHelper;
import utilities.RandomGenerator;

import static utilities.MathHelper.maxOf3;
import static utilities.RandomGenerator.getRandomNumber;

public class Exercise {
    public static void main(String[] args) {


        int num1 = getRandomNumber(5,8);
        int num2 = getRandomNumber(3,4);
        int num3 = getRandomNumber(10,12);

        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num3);

        System.out.println("The max is = " + maxOf3(num1,num2,num3));




    }



}
