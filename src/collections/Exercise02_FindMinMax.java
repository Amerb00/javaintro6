package collections;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class Exercise02_FindMinMax {
    public static void main(String[] args) {

        System.out.println(max(new Integer[] {10,100,123,507,25}));
        System.out.println(max(new Integer[] {5,-7,23}));
        System.out.println(max(new Integer[] {12}));


    }

    public static int max(Integer[] numbers){

        return new TreeSet<>(Arrays.asList(numbers)).last();
    }

    public static int secondMax(Integer[] numbers){

        return new TreeSet<>(Arrays.asList(numbers)).last();
    }
}
