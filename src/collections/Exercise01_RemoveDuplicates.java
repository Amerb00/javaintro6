package collections;

import java.util.*;

public class Exercise01_RemoveDuplicates {
    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>(Arrays.asList(2,3,4,5,5,2));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(10,10,10,10,10,10));
        List<Integer> list3 = new ArrayList<>(Arrays.asList(-3,-3,-5,0,0,0));

        System.out.println(removeDupe(list1));
        System.out.println(removeDupe(list2));
        System.out.println(removeDupe(list3));



    }

    public static HashSet<Integer> removeDupe(List<Integer> numbers){

        return new LinkedHashSet<>(numbers);



    }
}
