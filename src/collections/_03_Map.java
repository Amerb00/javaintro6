package collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class _03_Map {
    public static void main(String[] args) {

        /*
        Map is an interface and it has some implementation classes
        1. HashMap:
        2. LinkedHashMap :
        3. TreeMap:
         */

        System.out.println("=======HashMap=======");

        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Sandina", 25);
        hashMap.put("Zel", 20);
        hashMap.put("Assem", 27);
        hashMap.put("Assem", 24);
        hashMap.put("okan", 18);
        hashMap.put(null, 18);
        hashMap.put(null, 100);
        hashMap.put("", null);
        hashMap.put("Anton", null);
        hashMap.put("Jazzy", null);


        System.out.println(hashMap);
        System.out.println(hashMap.size());

        System.out.println("=======LinkedHasMap=======");

        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Sandina", 25);
        linkedHashMap.put("Zel", 20);
        linkedHashMap.put("Assem", 27);
        linkedHashMap.put("Assem", 24);
        linkedHashMap.put("okan", 18);
        linkedHashMap.put(null, 18);
        linkedHashMap.put(null, 100);
        linkedHashMap.put("", null);
        linkedHashMap.put("Anton", null);
        linkedHashMap.put("Jazzy", null);

        System.out.println(linkedHashMap);

        System.out.println("=======TreeMap=======");

        TreeMap<String, Integer> treeMap = new TreeMap<>();
        treeMap.put("Sandina", 25);
        treeMap.put("Zel", 20);
        treeMap.put("Assem", 27);


        System.out.println(treeMap);





    }
}
