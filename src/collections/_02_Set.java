package collections;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;

public class _02_Set {
    public static void main(String[] args) {
        /*
        Set is an interface, and it has some class implementation as below
            -Set does not keep insertion order
            -Set does not allow duplicates
            -Set allow ONLY 1 null element

        1. HashSet: The most common Set implementation
                -It does not keep insertion order
                -It does not allow duplicates
                -It allows ONLY 1 null element
        2. LinkedHashSet
                -It keep insertion order
                -It does not allow duplicates
                -It allows ONLY 1 null element
        3. TreeSet


         */

        HashSet<String> objects = new HashSet<>();


        objects.add(null);
        objects.add("Sandina");
        objects.add(null);
        objects.add("Okan");
        objects.add("Alex");
        objects.add("John");



        System.out.println(objects);
        HashSet<String> objects2 = new HashSet<>();

        objects2.add(null);
        objects2.add("Sandina");
        objects2.add(null);
        objects2.add("Okan");
        objects2.add("Alex");
        objects2.add("John");

        System.out.println(objects2);

        System.out.println("--------linkedHashSet--------");

        LinkedList<String> object2 = new LinkedList<>();
        object2.add(null);
        object2.add("Sandina");
        object2.add(null);
        object2.add("Okan");
        object2.add("Alex");
        object2.add("John");

        System.out.println(object2);

        System.out.println("--------TreeSet--------");

        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add(null);


        System.out.println(treeSet);






    }
}
