package collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class _01_List {
    public static void main(String[] args) {
        /*
        Common features:

        -They keep insertion order

         */


        ArrayList<String> list = new ArrayList<>();
        list.add("Carmela");
        list.add("Carmela");
        list.add("Zel");
        list.add("Zel");
        list.add("Okan");
        list.add(null);
        list.add(null);
        list.add(null);


        System.out.println(list);

        System.out.println("-----------ArrayList and LinkedList in the shape of a list-----------");
        List<Integer> numbers1 = new ArrayList<>();
        List<Integer> numbers2 = new LinkedList<>();



        System.out.println("-----------ArrayList and LinkedList in the shape of a list-----------");



    }
}
