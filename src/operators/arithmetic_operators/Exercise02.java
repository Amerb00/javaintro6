package operators.arithmetic_operators;

public class Exercise02 {
    public static void main(String[] args) {


        int month, week, biWeek;


        month = 90_000 / 12;
        week = 90_000/ 52;
        biWeek = 90_000/ 26;


        System.out.println("Monthly =  $" +month);
        System.out.println("Weekly =  $" + week);
        System.out.println("Bi-weekly =  $" + biWeek);
    }
}
