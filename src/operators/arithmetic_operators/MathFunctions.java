package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {

        int num1 = 9, num2 = 3;

        int sum = num1 + num2;

        System.out.println(sum);

        int multiple = 9 * 3;
        int sub = 9 - 3;
        int division = 9 / 3;
        int remainder = 9 % 3;

        System.out.println(multiple);
        System.out.println(sub);
        System.out.println(division);
        System.out.println(remainder);

    }
}
