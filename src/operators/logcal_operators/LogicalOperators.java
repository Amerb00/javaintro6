package operators.logcal_operators;

public class LogicalOperators {
    public static void main(String[] args) {

        System.out.println(true || false || false || true);
        System.out.println(true);

        System.out.println(2 == 2 && false != true);

        System.out.println((true || 5 < 3 ) && !(5 == 5 && 2 <0));

        System.out.println(!((5 * 20 == 50 && !true) || !(5 > 10 || false || !true )));

    }
}
