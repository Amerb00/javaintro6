package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your balance: ");
        double money = input.nextDouble();

        System.out.println("The initial balance = $" + money);

        System.out.println("What is the first transaction amount?");
        double firstTrns = input.nextDouble();

        money -= firstTrns;

        System.out.println("The balance after first transaction = $" + money);

        System.out.println("What is the second transaction amount?");
        double secondTrns = input.nextDouble();

        money -= secondTrns;


        System.out.println("The balance after second transaction = $" + money);

        System.out.println("What is the second transaction amount?");
        double thirdTrns = input.nextDouble();

        money -= thirdTrns;

        System.out.println("The balance after third transaction = $" + money);


    }
}
