package operators.shorthand_operators;

public class Exercise01 {
    public static void main(String[] args) {

        int a ,b;

        a = 8;
        b = 3;

        int sum1 = a += b;
        int sum2 = a -= b;
        int sum3 = a *= b;
        int sum4 = a /= b;
        int sum5 = a %= b;

        System.out.println(sum1);
        System.out.println(sum2);
        System.out.println(sum3);
        System.out.println(sum4);
        System.out.println(sum5);


    }
}
