package casting;

import java.util.Scanner;

public class ImplicitCasting {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1,num2,num3;

        System.out.println("Enter 3 numbers");
        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();


        if (num2 == num3 && num1 == num3){
            System.out.println("TRIPLE MATCH");
        } else if (num1 == num2) {
            System.out.println("DOUBLE MATCH");
        }else if (num2 == num3){
            System.out.println("DOUBLE MATCH");
        }else if (num1 == num3){
            System.out.println("DOUBLE MATCH");
        }else {
            System.out.println("NO MATCH");
        }


    }

}

