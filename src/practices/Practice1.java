package practices;

import utilities.RandomGenerator;

public class Practice1 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        int rando = RandomGenerator.getRandomNumber(0,50);

        System.out.println("The " + rando + " * 5 = " + rando*5);

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        rando = RandomGenerator.getRandomNumber(1,10);
        int rando2 = RandomGenerator.getRandomNumber(1,10);

        System.out.println("Min number = " + Math.min(rando2,rando));
        System.out.println("Max number = " + Math.max(rando2,rando));
        System.out.println("Difference = " + (Math.max(rando2,rando) - Math.min(rando2,rando)));


        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        rando = RandomGenerator.getRandomNumber(50,100);

        System.out.println("The " + rando + " % 10 = " + rando % 10);



        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        rando = RandomGenerator.getRandomNumber(1,10);
        rando2 = RandomGenerator.getRandomNumber(1,10);
        int rando3 = RandomGenerator.getRandomNumber(1,10);
        int rando4 = RandomGenerator.getRandomNumber(1,10);
        int rando5 = RandomGenerator.getRandomNumber(1,10);

        int points = 0;
        points += rando * 5;
        points += rando2 * 4;
        points += rando3 * 3;
        points += rando4 * 2;
        points += rando5;

        System.out.println(points);


        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        rando = RandomGenerator.getRandomNumber(1,25);
        rando2 = RandomGenerator.getRandomNumber(26,50);
        rando3 = RandomGenerator.getRandomNumber(51,75);
        rando4 = RandomGenerator.getRandomNumber(76,100);


        System.out.println("Difference of max and min = " + (Math.max(Math.max(rando5,rando4),Math.max(rando2,rando)) - Math.min(Math.min(rando5,rando4),Math.min(rando2,rando))) );
        System.out.println("Difference of second and third = ");
        System.out.println("Average of all  = " + ((rando4+rando5+rando3+rando+rando2)/4 ));




    }
}
