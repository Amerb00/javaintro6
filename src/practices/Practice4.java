package practices;

import utilities.ScannerHelper;

public class Practice4 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        FooBar();
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        moreThan9();
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        largerNum();
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(countVowel());
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(fibonacci());
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        System.out.println(moreThan10());

    }

    public static void FooBar(){

        for (int i = 1; i <= 10; i++) {
            if (i % 10 == 0) System.out.println("FooBar");
            else if (i % 2 == 0) System.out.println("Foo");
            else if (i % 5 == 0) System.out.println("Bar");
            else System.out.println(i);
        }

    }

    public static void moreThan9(){
        int number = 0;

        do {
            number = ScannerHelper.getNumber();
            if (number >= 10) System.out.println("This number is more than or equals 10");
            else System.out.println("This number is not more than or equals 10");
        }
       while (number <= 10);





    }

    public static void largerNum(){
        int num = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        for (int i = Math.min(num,num2); i <= Math.max(num,num2) ; i++) {
            if (!(i == 5))System.out.println(i);

        }


    }

    public static int countVowel(){
        String str = ScannerHelper.getString();

        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.toLowerCase().charAt(i) == 'a' ||str.toLowerCase().charAt(i) == 'e' ||
                    str.toLowerCase().charAt(i) == 'i' ||str.toLowerCase().charAt(i) == 'o' ||
                    str.toLowerCase().charAt(i) == 'u') count++;
        }
        return count;

    }

    public static String fibonacci(){
        
        int num = ScannerHelper.getNumber();

        int first = 0;
        int second = 1;
        String fib = "";


        System.out.print("" + first + "-" + second + "-");

        for (int i = 0; i < num; i++) {
            int thirdNum = first + second;
            fib += thirdNum + "-";
            first = second;
            second = thirdNum;
        }
        return fib;
    }

    public static String moreThan10(){

        int num = 0;
        int count = 0;
        do {
            num += ScannerHelper.getNumber();
            count++;
        }
        while (num < 100);
        if (count >1) return "Sum of the entered number is at least 100";
        return ("This number is already more than 100");

    }


}
