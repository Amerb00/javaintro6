package practices;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Test {

    public static void main(String[] args) {

        int[] nums = {1,2,3,4,100};


        int max = 0;
        int min = Integer.MAX_VALUE;
        int mean = 0;

        for(int num : nums){
            if(num < min) min = num;
            if(num > max) max = num;
        }
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] == max) nums[i]  = 0;
            if(nums[i]  == min) nums[i]  = 0;
        }


        for(int num : nums){
            mean += num;
        }
        System.out.println(mean/ nums.length);
        


    }

    public static void moreThan100() {

        Scanner input = new Scanner(System.in);
        int count = 0;
        int num = 0;

        do {
            System.out.println("Please enter a number");
            num += input.nextInt();
            count++;
        }
        while (num < 100);

        if (count > 1) System.out.println("The sum of the numbers is at least 100");
        else System.out.println("This number is already more than 100");

    }

    public static int[] getFib() {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the length of Fibonacci");

        int num = input.nextInt();
        int[] arr = new int[num];
        int num1 = 0, num2 = 1, sum;
// 0 1 1 2 3 5 8

        for (int i = 0; i < num; i++) {
            arr[i] = num1;
            sum = num1 + num2;
            num1 = num2;
            num2 = sum;
        }
        return arr;


    }
}
