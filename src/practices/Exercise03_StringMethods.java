package practices;

import utilities.ScannerHelper;

public class Exercise03_StringMethods {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();

        if (str.length() >= 4) {
            System.out.println("First 2 characters are = " + str.substring(0, 2));
            System.out.println("Last 2 characters are = " + str.substring(str.length() - 2));
            System.out.println("The other characters are = " + str.substring(2, str.length() - 2));
        }
        else System.out.println("INVALID CHARACTERS");


    }
}
