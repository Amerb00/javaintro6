package practices;

public class Practice3 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        split("Javascript");
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        first2("xasdas");
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(removeFirst2("yellow","red"));
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        startsXX("xxb");
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        lengthChar("JavaScript");
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        System.out.println(middle("to"));

    }

    public static void lengthChar(String str){

        if (str.length() == 0) System.out.println("length is zero");
        else {
            System.out.println("Length is = "+ str.length());
            System.out.println("First char is = "+ str.charAt(0));
            System.out.println("Last char is = "+ str.charAt(str.length()-1));
            if (str.toLowerCase().contains("a")||str.toLowerCase().contains("3")||str.toLowerCase().contains("i")
                    ||str.toLowerCase().contains("o")||str.toLowerCase().contains("u")) System.out.println("This String has vowel");
            else System.out.println("This String does not hav vowel");
        }


    }

    public static String middle(String str){
        if (str.length() > 3) {
            if (str.length() % 2 == 0) return str.substring(str.length() / 2 - 1, str.length() / 2 + 1);
            else return str.substring(str.length() / 2, str.length() / 2 + 1);
        }
       return "Length is less than 3";
    }

    public static void split(String str){

        if (str.length() < 4) System.out.println("INVALID INPUT");
        else {
            System.out.println("First 2 characters are = " + str.substring(0, 2));
            System.out.println("Last 2 characters are = " + str.substring(str.length() - 2));
            System.out.println("The other characters = " + str.substring(2, str.length() - 2));
        }

    }

    public static void first2(String str){

        if (str.length() < 2) System.out.println("Length is less than 2");
        else System.out.println(str.substring(0, 2).equals(str.substring(str.length() - 2)));


    }

    public static String removeFirst2(String str,String str2){


        if (str.length() < 2 || str2.length() < 2) return "Invalid Input!";

        return str.substring(1,str.length()-1) + str2.substring(1,str2.length()-1);






    }

    public static void startsXX(String str){

        if (str.length() < 4) System.out.println("Invalid input");
        else System.out.println(str.startsWith("xx") && str.endsWith("xx"));



    }






}
