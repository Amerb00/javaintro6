package practices;

import utilities.RandomGenerator;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice5 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        int[] arr = {0,-4,-7,0,5,10,45};
        findPosAndNeg(arr);
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        findLongestAndShortest(new String[]{"blue","red","yellow","white"});
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        maxMin();
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        maxMinWithSort();
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(containsApple(new String[]{"banana", "orange", "Apple"}));

        // ____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");
        matchingElements(new int[]{10, 20 ,20, 30, 50, 70}, new int[]{20,20,50,70,100,300});
        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        dupeChar("baNana");
        //____________________________________________________________TASK8_____________________________________________________________________________
        System.out.println("----------TASK8----------");
        System.out.println(countDupe(new int[]{2,3,7,1,1,7,1}));
    }

    public static void findPosAndNeg(int[] arr){


        for (int i : arr) {
        if (i > 0) {
            System.out.println("First Positive number is " + i);
            break;
            }

        }

        for (int i : arr) {
            if (i < 0) {
                System.out.println("First Negative number is " + i);
                break;
            }

        }



    }

    public static void findLongestAndShortest(String[] arr){


        String longest = arr[0];
        String shortest = arr[0];



            for (String s1 : arr) {
                if (shortest.length() > s1.length())
                    shortest = s1;
            }
            for (String s1 : arr) {
                if (longest.length() < s1.length())
                    longest = s1;

            }


        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);
    }

    public static void maxMin(){


        int[] arr = new int[5];
        for (int i = 0; i < 5; i++) {
            int rando = RandomGenerator.getRandomNumber(1,10);
            arr[i] = rando;
        }
        int max =0;
        int min = 10;
        for (int i : arr) {
            if (i > max)
                max =i;
            if(i < min)
                min = i;
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("Max "+max);
        System.out.println("Min "+min);


    }

    public static void maxMinWithSort(){

        int[] arr = new int[5];
        for (int i = 0; i < 5; i++) {
            int rando = RandomGenerator.getRandomNumber(1,10);
            arr[i] = rando;
        }
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println("Min "+arr[0]);
        System.out.println("Max "+arr[arr.length-1]);



    }

    public static boolean containsApple(String[] arr){

        for (String s : arr) {
            if (s.equalsIgnoreCase("apple"))return true;
        }
        return false;
    }

    public static void matchingElements(int[]a , int[]b){

        ArrayList<Integer> c = new ArrayList<>();


        for (int i : a) {
            for (int i1 : b) {
                if (i == i1 && !(c.contains(i))) c.add(i);
            }
        }
        for (Integer integer : c) {
            System.out.println(integer);
        }



    }

    public static void dupeChar(String str){


        ArrayList<Character> dupe = new ArrayList<>();// [a,N]

        //baNana

        for (int i = 0; i < str.length() / 2; i++) {
            for (int j = str.length()- 1 ; j > str.length()/2  ; j--) {
                if (str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j) && !(dupe.contains(str.charAt(i)))) dupe.add(str.charAt(i));
            }
        }

        for (Character character : dupe) {
            System.out.println(character);

        }





    }

    public static int countDupe(int[] arr){

        // 2,3,7,1,1,7,1


        int count=0;
        ArrayList<Integer> dupe = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length ; j++) {
                if (arr[i] == arr[j] && !(dupe.contains(arr[i]))) {
                    dupe.add(arr[i]);
                    count++;
                }
            }
        }


        return count;


    }







}
