package practices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Practice6 {
    public static void main(String[] args) {


        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(double1(new int[] {3, 2, 5, 7, 0}));
        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2,3,7,1,1,7,1)) ));
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2,3,7,1,1,7,1)) ));
        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("", "tech", "" ,null))));
        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200,500,5,100,99,101,75))));
        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        System.out.println(uniquesWords("Star Light Star Bright"));
        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        triagnle();
        //____________________________________________________________TASK7_____________________________________________________________________________
        System.out.println("----------TASK7----------");
        fib(5);



    }

    public static ArrayList<Integer> double1(int[] arr){

        ArrayList<Integer> nums = new ArrayList<>();

        for (int i : arr) {
             nums.add(i * 2);
        }
        return nums;



    }

    public static int secondMax(ArrayList<Integer> list){

        Collections.sort(list);

        int max = 0;
        for (int i : list) {
            if (i > max) {
                max = i;
            }
        }

        for (int i = list.size()-1; i > 0; i--) {
            if (list.get(i) < max) return list.get(i);
            
        }
        return 0;
    }

    public static int secondMin(ArrayList<Integer> list){

        Collections.sort(list);
        int min = Integer.MAX_VALUE;
        for (Integer integer : list) {
            if (integer < min)
                min = integer;

        }


        for (Integer integer : list) {
            if (integer > min){
                min = integer;
                return min;
            }

        }
        return 0;



    }

    public static ArrayList<String> removeEmpty(ArrayList<String> list){

        list.removeIf(element -> element == null || element.equals(" "));

        return list;


    }

    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list){

        list.removeIf(element -> element >= 100 || element <= -100);

        return list;



    }

    public static ArrayList<String> uniquesWords(String str){

        ArrayList<String> dupe = new ArrayList<>();

        for (String i : str.split(" ")) {
            if (!(dupe.contains(i))) dupe.add(i);

        }
        return dupe;

    }

    public static void triagnle(){

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("A");

            }
            System.out.println();
        }



    }

    public static void fib(int num){

        ArrayList<Integer> fib = new ArrayList<>();

        //0 1 1 2 3 5 8 13 21

        int sum,first =0,second=1;

        for (int i = 0; i < num; i++) {
            fib.add(first);
            sum = first + second;
            first = second;
            second = sum;
        }
        System.out.println(fib);

    }








}
