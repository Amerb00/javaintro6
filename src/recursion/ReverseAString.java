package recursion;

public class ReverseAString {

    public static void main(String[] args) {

        System.out.println(reverseSting("Hello"));
        System.out.println(reverseSting("Java"));
    }

    public static String reverseSting(String str){
        if (str.length() <= 1) return str;
        return reverseSting(str.substring(1)) + str.charAt(0);
    }
}
