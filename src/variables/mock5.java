package variables;

import java.util.HashMap;
import java.util.Map;

public class mock5 {
    public static void main(String[] args) {
        System.out.println(fruitCounter(new String[] {"Apple","Apple","Orange","Orange","bannana"}));

    }

    public static HashMap<String,Integer> fruitCounter(String[] arr){

        HashMap<String,Integer> fruits = new HashMap<>();
        /*
        {"Apple","Apple","Orange"}
         */

        for (String string : arr) {
            if (fruits.containsKey(string)){
                fruits.put(string,fruits.get(string) + 1);
            }
            else fruits.put(string,1);
        }
        return fruits;

    }
}
