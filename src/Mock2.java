import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Mock2 {

    public static void main(String[] args) {
        System.out.println(getFactorial(5));
        System.out.println(Arrays.toString(removeAllDupe(new String[]{"foo", "bar", "foo", "123", "bar"}))); // [bar, 123, foo]

        System.out.println(Arrays.toString(fib(5)));


    }

    public static int getFactorial(int num){

        int fac = num;
        if (num > 0){
            for (int i = 2; i < num ; i++) {
                fac *= i;
            }
            return fac;

        }
        return 1;
    }

    public static String[] removeAllDupe(String[] arr){

        ArrayList<String> empty = new ArrayList<>();

        for (String s : arr) {
            if (!empty.contains(s)) empty.add(s);
        }

        return empty.toArray(new String[0]);


    }

    public static int[] fib(int num){

//        0-1-1-2-3-5-8-13
        int first =0;
        int second =1;
        int sum =1;

        int[] arr = new int[num];

        for (int i = 0; i < num; i++) {
            arr[i] = first;
            sum = first + second;
            first = second;
            second = sum;
        }
        return arr;








    }

}
