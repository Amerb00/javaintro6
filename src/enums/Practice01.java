package enums;

public class Practice01 {
    public static void main(String[] args) {


        DaysOfWeeks dayByUser = DaysOfWeeks.FRIDAY;

        switch (dayByUser){
            case SATURDAY:
            case SUNDAY:
                System.out.println("it is OFF today");
                break;
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                System.out.println("it is work day");
                break;
            default:
                throw new RuntimeException("No such enum value!!!");
        }
    }
}
