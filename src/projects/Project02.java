package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        //____________________________________________________________TASK1_____________________________________________________________________________

        System.out.println("----------TASK1----------");

        int num1, num2, num3;

        System.out.println("Please enter 3 numbers");

        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();

        int total = num1 * num2 * num3;

        System.out.println("The product of the numbers entered is = " + total);

        //____________________________________________________________TASK2_____________________________________________________________________________

        System.out.println("----------TASK2----------");

        System.out.println("What is your first name?");
        String fName = input.next();
        input.nextLine();

        System.out.println("What is your last name?");
        String lName = input.next();
        input.nextLine();

        System.out.println("What is your year name?");
        int age = 2023 - input.nextInt();
        input.nextLine();


        System.out.println(fName + " " + lName + " age is = " + age + ".");

        //____________________________________________________________TASK3_____________________________________________________________________________

        System.out.println("----------TASK3----------");


        System.out.println("What is your full name?");
        String name = input.nextLine();

        System.out.println("What is your weight?");
        num1 = input.nextInt();
        input.nextLine();

        double weight = (num1 * 2.205);

        System.out.println(name + " weight is = " + weight + ".");

        //____________________________________________________________TASK4_____________________________________________________________________________

        System.out.println("----------TASK4----------");


        System.out.println("What is your full name?");
        String name1 = input.nextLine();

        System.out.println("What is your age name?");
        int age1 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String name2 = input.nextLine();

        System.out.println("What is your age name?");
        int age2 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String name3 = input.nextLine();

        System.out.println("What is your age name?");
        int age3 = input.nextInt();
        input.nextLine();


        System.out.println(name1 + " age is " + age1 + ".");
        System.out.println(name2 + " age is " + age2 + ".");
        System.out.println(name3 + " age is " + age3 + ".");
        System.out.println("The average age is " + (( age1 + age2 + age3 ) / 3 ) + ".");
        System.out.println("The eldest age is " + Math.max(Math.max( age1 , age2 ), age3 ) + ".");
        System.out.println("The youngest age is " + Math.min(Math.min(age1 , age2), age3) + ".");





    }

}
