package projects;

import utilities.MathHelper;
import utilities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________

        System.out.println("----------TASK1----------");

        String sentence = ScannerHelper.getString();
        int count =0;

        if (!sentence.contains(" ")) System.out.println("This sentence does not have multiple words.");
        else {
            for (int i = 0; i < sentence.length(); i++) {
                if (Character.isWhitespace(sentence.charAt(i))) count++;
            }
            System.out.println("This sentence has " + (count + 1) + " words.");
        }


//____________________________________________________________TASK2_____________________________________________________________________________

        System.out.println("----------TASK2----------");

        Random random = new Random();

        int rando1 = random.nextInt(26);
        int rando2 = random.nextInt(26);

        for (int i = Math.min(rando1,rando2); i <= Math.max(rando1,rando2) ; i++) {
            if(Math.max(rando1,rando2) == i) System.out.print(i);
            else System.out.print(i + " - ");
        }

        //____________________________________________________________TASK3_____________________________________________________________________________

        System.out.println("----------TASK3----------");

        String str = ScannerHelper.getString();
        int counter = 0;

        if (str.length()-1 < 0) System.out.println( "This sentence does not have any characters.");
        else {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == 'a'|| str.charAt(i) == 'A') counter++;
            }
            System.out.println("This sentence has " + counter + " a or A letters.");
        }

        //____________________________________________________________TASK4_____________________________________________________________________________

        System.out.println("\n----------TASK4----------");


        String str2 = ScannerHelper.getString();

        String reverse = "";

        if (str2.length()-1 < 0) System.out.println( "This word does not have 1 or more characters.");
        else {
            for (int i = str2.length() -1; i >= 0; i--) {
                reverse += str2.charAt(i);
            }
            if (str2.equals(reverse)) System.out.println("This word is palindrome");
            else System.out.println("This word is not palindrome");
        }

        //____________________________________________________________TASK5_____________________________________________________________________________

        System.out.println("----------TASK5----------");

        int rows = 9;
        int space = rows - 1;
        int star = 1;
        for (int i = 0; i < rows ; i++) {

            for (int j = 0; j < space; j++) {
                System.out.print("   ");
            }
            for (int k = 0; k < star; k++) {
                System.out.print(" * ");
            }
        System.out.println();
        space--;
        star +=2;
        }
    }
}
