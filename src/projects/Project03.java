package projects;

import java.util.Random;

public class Project03 {
    public static void main(String[] args) {

        //____________________________________________________________TASK1_____________________________________________________________________________

        System.out.println("----------TASK1----------");

        String s1 = "24", s2 = "5";

        int num1 = Integer.parseInt(s1);
        int num2 = Integer.parseInt(s2);

        System.out.println("The sum of " + s1 + " and " + s2 + " = " + (num1 + num2));
        System.out.println("The subtraction of " + s1 + " and " + s2 + " = " + (num1 - num2));
        System.out.println("The division of " + s1 + " and " + s2 + " = " + (double) num1 / num2);
        System.out.println("The multiplication of " + s1 + " and " + s2 + " = " + (num1 * num2));
        System.out.println("The remainder of " + s1 + " and " + s2 + " = " + (num1 % num2));

        //____________________________________________________________TASK2_____________________________________________________________________________

        System.out.println("----------TASK2----------");

        Random random = new Random();

        int rando = random.nextInt(35) + 1;

        if (rando == 2 || rando == 3 || rando == 5 || rando == 7 || rando == 11 || rando == 13 || rando == 17 || rando == 19 || rando == 23 || rando == 29 || rando == 31) {
            System.out.println(rando + " IS A PRIME NUMBER");
        } else System.out.println(rando + " IS NOT A PRIME NUMBER");


        //____________________________________________________________TASK3_____________________________________________________________________________

        System.out.println("----------TASK3----------");

        int rando1 = random.nextInt(50) + 1;
        int rando2 = random.nextInt(50) + 1;
        int rando3 = random.nextInt(50) + 1;


        System.out.println("Lowest number is- = " + Math.min(Math.min(rando1, rando2), rando3));

        if ((rando1 < rando2 && rando2 < rando3) || (rando3 < rando2 && rando2 < rando1)) {
            System.out.println("Middle number is = " + rando2);
        } else if ((rando2 < rando1 && rando1 < rando3) || (rando3 < rando1 && rando1 < rando2)) {
            System.out.println("Middle number is = " + rando1);
        } else {
            System.out.println("Middle number is = " + rando3);
        }


        System.out.println("Greatest number is = " + Math.max(Math.max(rando1, rando2), rando3));


        //____________________________________________________________TASK4_____________________________________________________________________________

        System.out.println("----------TASK4----------");


        char a = 'b';

        if (a >= 65 && a <= 90) {

            System.out.println("The letter is uppercase");

        } else if (a >= 97 && a <= 122) {

            System.out.println("The letter is lowercase");

        } else System.out.println("Invalid character detected!!!");

        //____________________________________________________________TASK5_____________________________________________________________________________

        System.out.println("----------TASK5----------");

        char b = '2';


        if (b == 65 || b == 69 || b == 73 || b == 79 || b == 85 || b == 97 || b == 101 || b == 105 || b == 111 || b == 117) {
            System.out.println("The letter is vowel");

        } else if (b >= 65 && b <= 90 || b >= 97 && b <= 122) {

            System.out.println("The letter is a consonant ");

        } else System.out.println("Invalid character detected!!!");

//____________________________________________________________TASK6_____________________________________________________________________________

        System.out.println("----------TASK6----------");


        char c = 'a';

        if (c >= 65 && c <= 90 || c >= 97 && c <= 122) {

            System.out.println("Invalid character detected!!! ");
        } else if (c >= 48 && c <= 57) {
            System.out.println("Invalid character detected!!!");

        } else System.out.println("Special character is = " + c);


        //____________________________________________________________TASK7_____________________________________________________________________________

        System.out.println("----------TASK7----------");


        char d = 'a';

        if (d >= 65 && d <= 90 || d >= 97 && d <= 122) {

            System.out.println("Character is a letter " + d);
        } else if (d >= 48 && d <= 57) {
            System.out.println("Character is a digit " + d);

        } else System.out.println("Character is a special character " + d);


    }
}
