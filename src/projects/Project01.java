package projects;

public class Project01 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");

        String name = "Belal"; // My name is being stored in the variable "name"
        System.out.println("My name is " + name + ".");// The variable "name" is being concatenated

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");

        //Declaring variables to char
        char nameCharacter1, nameCharacter2, nameCharacter3, nameCharacter4, nameCharacter5;

        //Assigning
        nameCharacter1 = 'B';
        nameCharacter2 = 'e';
        nameCharacter3 = 'l';
        nameCharacter4 = 'a';
        nameCharacter5 = 'l';

        //Printing and concatenating
        System.out.println("Name letter 1 is " + nameCharacter1);
        System.out.println("Name letter 2 is " + nameCharacter2);
        System.out.println("Name letter 3 is " + nameCharacter3);
        System.out.println("Name letter 4 is " + nameCharacter4);
        System.out.println("Name letter 5 is " + nameCharacter5);
        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        //Declaring variables to String
        String myFavMovie, myFavSong, myFavCity, myFavActivity, myFavSnack;

        //Assigning
        myFavMovie = "Batman Dark Knight";
        myFavSong = "505"; //This is the name of the song
        myFavCity = "Chicago";
        myFavActivity = "Playing volleyball";
        myFavSnack = "Cheez it";

        //Printing and concatenating
        System.out.println("My favorite movie is " + myFavMovie + ".");
        System.out.println("My favorite song is " + myFavSong + ".");
        System.out.println("My favorite city is " + myFavCity + ".");
        System.out.println("My favorite activity is " + myFavActivity + ".");
        System.out.println("My favorite snack is " + myFavSnack + ".");

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");

        //Declaring variables to int
        int myFavNumber, numberOfStatesVisited, numberOfCountriesIVisited;

        //Assigning
        myFavNumber = 2;
        numberOfStatesVisited = 5;
        numberOfCountriesIVisited = 4;

        //Printing and concatenating
        System.out.println("My favorite number is " + myFavNumber + ".");
        System.out.println("I have visited " + numberOfStatesVisited + " states" + ".");
        System.out.println("I have visited " + numberOfCountriesIVisited + " countries" + ".");

        //____________________________________________________________TASK5_____________________________________________________________________________
        System.out.println("----------TASK5----------");

        //Declaring and assigning
        boolean amiAtSchoolToday = false;

        //Printing and concatenating
        System.out.println("I am at school today " + amiAtSchoolToday + '.');

        //____________________________________________________________TASK6_____________________________________________________________________________
        System.out.println("----------TASK6----------");

        //Declaring and assigning
        boolean isWeatherNiceToday = false;

        //Printing and concatenating
        System.out.println("Weather is nice today " + isWeatherNiceToday + ".");


    }
}
