package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        //____________________________________________________________TASK1_____________________________________________________________________________
        System.out.println("----------TASK1----------");
        System.out.println(findClosetDistance(new int[]{10, -5, 20, 50, 100}));

        //____________________________________________________________TASK2_____________________________________________________________________________
        System.out.println("----------TASK2----------");
        System.out.println(findSingleNumber(new int[]{7, 5,-1, 3, 5,3,-1}));

        //____________________________________________________________TASK3_____________________________________________________________________________
        System.out.println("----------TASK3----------");

        System.out.println(findFirstUniqueCharacter("abc abc d"));

        //____________________________________________________________TASK4_____________________________________________________________________________
        System.out.println("----------TASK4----------");
        System.out.println(findMissingNumber(new int[] {7,8,6,4}));
    }

    public static int findClosetDistance(int[] arr){

        int smallest = Integer.MAX_VALUE;
        if (arr.length > 1){
            for (int i = 0; i < arr.length; i++) {
                for (int j = i+1; j < arr.length ; j++) {
                    if (Math.abs(arr[i] - arr[j]) < smallest) smallest = Math.abs(arr[i]-arr[j]);
                }
            }
            return smallest;
        }
        return -1;

    }

    public static int findSingleNumber(int[] arr){

        String empty = "";
        if (arr.length < 2) return arr[0];
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if ((arr[i] == arr[j])) {
                    empty += arr[i];
                    break;
                }
            }
        }
        for (int count : arr) {
            if(!empty.contains(""+count)) return count;
        }
        return -1;
    }

    public static char findFirstUniqueCharacter(String str){

        for (int i = 0; i < str.length(); i++) {
            boolean unique = true;
            for (int j = 0; j < str.length(); j++) {
                if (i !=j && str.charAt(i) == str.charAt(j)) {
                    unique = false;
                    break;
                }
            }
            if (unique){
                return str.charAt(i);
            }
        }

       return ' ';



    }

    public static int findMissingNumber(int[] arr){

        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            if (Math.abs(arr[i] - arr[i+1]) != 1) return arr[i] +1;
        }
        return 0;

    }


}
