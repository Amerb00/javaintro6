package projects;

import utilities.ScannerHelper;

import java.util.StringTokenizer;

public class Project04 {
    public static void main(String[] args) {



        //____________________________________________________________TASK1_____________________________________________________________________________

        System.out.println("----------TASK1----------");

        String str = ScannerHelper.getString();
        System.out.println(str.length() >= 8 ? str.substring(str.length()-4) + str.substring(4, str.length() -4 )
                + str.substring(0,4):"This String does not have 8 characters");

        //____________________________________________________________TASK2_____________________________________________________________________________

        System.out.println("----------TASK2----------");

        String str2 = ScannerHelper.getString();

        int index = str2.lastIndexOf(" ") + 1;
        System.out.println(str2.contains(" ") ? str2.substring(index) + str2.substring(str2.indexOf(" "),index)
                + str2.substring(0,str2.indexOf(" ")) : "This sentence does not have 2 or more words to swap" );
        //____________________________________________________________TASK3_____________________________________________________________________________

        System.out.println("----------TASK3----------");

        String str3 = ScannerHelper.getString();


        System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));

        //____________________________________________________________TASK4_____________________________________________________________________________

        System.out.println("----------TASK4----------");

        String name = ScannerHelper.getFirstName();

        if(name.length() >=2 )
            if (name.length() % 2 == 0)
                System.out.println(name.substring(name.length()/2 -1 , name.length()/2 +1 ));
        else System.out.println(name.charAt(name.length()/2));

        else System.out.println("INVALID INPUT");
//____________________________________________________________TASK5_____________________________________________________________________________

        System.out.println("----------TASK5----------");


        String country = ScannerHelper.getFavCountry();

        System.out.println((country.length() >= 5) ? country.substring(2,country.length() -2) : "INVALID INPUT!!!");

//____________________________________________________________TASK6_____________________________________________________________________________

        System.out.println("----------TASK6----------");


        String address = ScannerHelper.getAddress();

        System.out.println(address
                .replace('a', '@').replace('A', '@')
                .replace('e', '#').replace('E', '#')
                .replace('i','+').replace('I','+')
                .replace('o', '@').replace('O', '@')
                .replace('u', '$').replace('U', '$'));
//____________________________________________________________TASK7_____________________________________________________________________________

        System.out.println("----------TASK7----------");

        String word = ScannerHelper.getString();
        StringTokenizer count = new StringTokenizer(word);

        System.out.println(word.contains(" ") ? "This sentence has " + count.countTokens() + " words.": "This sentence does not have multiple words." );
    }























}
