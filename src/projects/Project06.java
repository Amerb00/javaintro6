package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {
        int[] arr ={10,7,7,10,-3,10,3};
        int[] arr2 ={10,7,7,10,-3,10,3};
        int[] arr3 ={10,5,6,7,8,5,15,15};
        int[] arr4 ={10,5,6,7,8,5,15,15};
        String[] arr5={"foo","bar","Foo","bar","6","abc","6","xyz"};
        String[] arr6={"pen","eraser","pencil","pen","123","abc","pen","eraser"};

        System.out.println("----------TASK1----------");
        findGreatestAndSmallestWithSort(arr);
        System.out.println("----------TASK2----------");
        findGreatestAndSmallest(arr2);
        System.out.println("----------TASK3----------");
        findSecondGreatestAndSmallestWithSort(arr2);
        System.out.println("----------TASK4----------");
        findSecondGreatestAndSmallest(arr3);
        System.out.println("----------TASK5----------");
        findDuplicatedElementsInAnArray(arr5);
        System.out.println("----------TASK6----------");
        findMostRepeatedElementInAnArray(arr6);





    }

    public static void findGreatestAndSmallestWithSort(int[] array){

        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println("The Smallest = " + array[0]);
        System.out.println("The Greatest = " + array[array.length-1]);


    }

    public static void findGreatestAndSmallest(int[] array){

        int max = array[0];
        for (int i : array) {
            if (i > max) max = i;
        }
        int min = array[0];
        for (int i : array) {
            if (i < min) min =i;

        }
        System.out.println(Arrays.toString(array));
        System.out.println("The smallest = " + min);
        System.out.println("The Greatest = " +max);




    }

    public static void findSecondGreatestAndSmallestWithSort(int[] array){

        Arrays.sort(array);

        System.out.println(Arrays.toString(array));
        int secondMin = array[0];
        int secondMax = array[array.length-1];
        for (int i : array) {
            if (i > secondMin){
                secondMin= i;
                break;
            }
        }
        for (int i = array.length-1; i >= 0 ; i--) {
            if (array[i] < secondMax) {
                secondMax=array[i];
                break;
            }

        }
        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);

    }

    public static void findSecondGreatestAndSmallest(int[] array){


        int secondMin =0;
        int secondMax =0;
        int min = array[0];
        int max = array[0];
        System.out.println(Arrays.toString(array));
        // finds the smallest number
        for (int i : array) {
            if (i < min){
                secondMin = min;
                min = i;
            }
            // if the element in the array is not equal to the smallest then it will find the next smallest.
            else if (i != min && i < secondMin) {
                secondMin = i;
            }
        }
        //finds the largest number
        for (int i : array) {
            if (i > max){
                secondMax = max;
                max = i;
            }
            // if the element in the array is not equal to the largest then it will find the next largest.
            else if (i != max && i > secondMax) {
                secondMax = i;
            }
        }

        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);





    }

    public static void findDuplicatedElementsInAnArray(String[] array){

        /*
        First loop starts from the index of 0 of the array
        Second loop loops through the array comparing all elements from the element in the first loop
         */
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length; i++) {
            String dupe = array[i];
            for (int j = i + 1; j < array.length ; j++) {
                if (dupe.equals(array[j])) System.out.println(dupe);
            }
        }
    }

    public static void findMostRepeatedElementInAnArray(String[] array){

        System.out.println(Arrays.toString(array));

        int counter = 0;
        String repeated = "";

        for (String s : array) {
            int count = 0;// resets the counter every loop

            for (String s1 : array) {
                if (s.equals(s1)) count++;// counts how many times an element repeats
            }
            if (count > counter){// only the highest count of repeats will switch the repeated variable to the most repeated
                counter = count;
                repeated =s;
            }

        }
        System.out.println(repeated);



    }


}





