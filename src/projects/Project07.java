package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("----------TASK1----------");
        System.out.println(countMultipleWords(new String[] {"foo",""," abc","foo bar","java is fun","ruby"}));
        System.out.println("----------TASK2----------");
        System.out.println(removeNegative(new ArrayList<>(Arrays.asList(2,-5,6,7,-10,-78,0,15))));
        System.out.println("----------TASK3----------");
        System.out.println(validatePassword("Abcc3413!"));
        System.out.println("----------TASK4----------");
        System.out.println(validateEmailAddress("cd@gg.com"));


    }

    public static int countMultipleWords(String[] arr){

        int counter = 0;
        for (String s : arr) {
            if (!(s.startsWith(" ")) && !(s.endsWith(" ")) && s.contains(" ")) counter++;
        }
        return counter;
    }

    public static ArrayList<Integer> removeNegative(ArrayList<Integer> list){

        list.removeIf(element -> element < 0);
        return list;

    }

    public static boolean validatePassword(String str){

        int upper = 0;
        int lower = 0;
        int digit = 0;
        int special = 0;


        if (!(str.contains(" ")) && str.length() >= 8 && str.length() <= 16){
            for (int i = 0; i < str.length(); i++) {
                if (Character.isUpperCase(str.charAt(i))) upper++;
                else if (Character.isLowerCase(str.charAt(i))) lower++;
                else if (Character.isDigit(str.charAt(i)))digit++;
                else if (!(Character.isLetterOrDigit(str.charAt(i))))special++;
            }

        }

        return  (upper >=1 && lower >=1 && digit >= 1 && special >=1);


    }

    public static boolean validateEmailAddress(String str){

        int countAtt = 0;
        int countP = 0;


        if (!(str.contains(" ")) && str.substring(0, str.indexOf("@")).length() >= 2
                && str.substring(str.indexOf("@"), str.indexOf(".")).length() > 2
                && str.substring(str.indexOf(".")).length() > 2){

            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == '@') countAtt++;
                if (str.charAt(i) == '.') countP++;

            }

            return countP == 1 && countAtt == 1;
        }
        return false;

    }


}


