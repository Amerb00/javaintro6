package strings;

public class Exercise01 {
    public static void main(String[] args) {

        String name = "John";
        String address = " Chicago IL 12345";

        System.out.println(name); // John
        System.out.println(address);// Chicago IL 12345

        String favMovie = "BatMan";
        System.out.println("My favorite movie = " + favMovie);
    }
}
